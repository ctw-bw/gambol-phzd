#ifndef ROBOTS_RAISIMROBOTMODEL_H_
#define ROBOTS_RAISIMROBOTMODEL_H_

#include <Eigen/Dense>
#include <vector>
#include <string>
#include "RobotModel.h"
#include <gambol/Variables/NodesHolder.h>
#include <raisim/World.hpp>

namespace gambol {

    /**
     * Robot model implemented through RaiSim articulated system
     *
     * The generalized coordinates are called that literally (q, qpos)
     * The generalized velocity is also called exactly that (dq, qvel)
     */
    class RaiSimRobotModel : public RobotModel {

    public:

        using Ptr = std::shared_ptr<RaiSimRobotModel>;

        /**
         * Regular constructor
         *
         * The end-effectors are defined by fame names (= joint names). It is most convenient to create a type-fixed joint
         * at each end-effector.
         *
         * @param urdf_file     Robot model file
         * @param size_q
         * @param size_dq
         * @param size_u
         * @param ee_names      List of names of end-effectors
         * @param S             Actuator selection matrix, @see actuator_selector_
         */
        RaiSimRobotModel(const std::string& urdf_file, int size_q,
                         int size_dq, int size_u,
                         const std::vector<std::string>& ee_names,
                         const Eigen::MatrixXd& S = Eigen::MatrixXd(0, 0));

        /** Copy constructor */
        RaiSimRobotModel(const RaiSimRobotModel&) = delete;

        /** Destructor */
        ~RaiSimRobotModel() override = default;

        /**
         * Clone instance
         *
         * A pointer of the base class is returned for interchangeability
         */
        RobotModel::Ptr clone() const override;

        /**
         * Set new selection matrix
         *
         * An error will be thrown if the matrix does not match.
         * Leave to argument empty to try to automatically set a matrix. This default matrix will assume the last #u
         * coordinates are actuated.
         *
         * @see RaiSimRobotModel::actuator_selector_
         */
        void SetSelectionMatrix(Eigen::MatrixXd S = Eigen::MatrixXd(0, 0));

        /**
         * Get forward dynamics
         *
         * RaiSim lacks a feature to compute the dynamics and return it,
         * without modifying the model. So instead we perform an integration
         * step, look at the difference and reset the word.
         * This method computes the current dynamics of the current state
         * of this->system_. It is up to the user whether system_ is actually
         * up-to-date with q_, dq_, etc.!
         */
        VectorXd GetDynamics() const override;

        /**
         * Get current dynamics jacobian w.r.t. position
         *
         * Calculated through finite difference.
         * This assumes the model and data are up-to-date through SetCurrent().
         */
        Jacobian GetDynamicsJacobianWrtPos() const override;

        /**
         * Get current dynamics jacobian w.r.t. velocity
         *
         * Calculated through finite difference.
         * This assumes the model and data are up-to-date through SetCurrent().
         */
        Jacobian GetDynamicsJacobianWrtVel() const override;

        /**
         * Get current dynamics jacobian w.r.t. torque
         *
         * Calculated through finite difference.
         * This assumes the model and data are up-to-date through SetCurrent().
         */
        Jacobian GetDynamicsJacobianWrtTorque() const override;

        /**
         * Get current dynamics jacobian w.r.t. forces
         *
         * The end-effector forces are stacked together, so this jacobian has dimensions:
         *      ( n_ee * 3 x n_dof )
         */
        Jacobian GetDynamicsJacobianWrtForces(uint ee_id) const override;

        /**
	     * Get position in world coordinates of end-effector
	     */
        Vector3d GetEEPos(uint ee_id) const override;

        /**
         * Get jacobian of end-effector position to joint positions
         */
        Jacobian GetEEPosJacobian(uint ee_id) const override;

        /**
         * Enable or disable collision detection between the robot and its surroundings
         *
         * @param enable
         */
        void EnableContact(bool enable);

        /**
         * Get reference to the raisim world object
         */
        raisim::World& GetWorld();

        /**
         * Get pointer to the raisim system object (the robot itself)
         */
        raisim::ArticulatedSystem* GetSystem();

        /**
         * Get pointer to ground object
         */
        raisim::Ground* GetGround();

        /**
         * Read joint limits from the system model
         *
         * The limits are determined by the `lower="..." upper="..."` clauses.
         * The lower and upper limits of each joint is listed. The size of the returned is list is the number of
         * generalized coordinates. If a joint has no limits, then <-1.0e19, 1.0e19> is filled in.
         * Pass a not-null pointer as an argument to also get a list of bounded joints.
         *
         * Use `is_3d` to skip trying to read the first 6 DOF (7 coordinates) and instead
         * consider them not limited.
         *
         * @param is_3d Do not consider limits for the first 6 DOFs
         * @param[out] bounds Return a list of the joints that are actually limited.
         * @return List of pairs representing limits
         */
        std::vector<std::pair<double, double>> GetJointLimits(bool is_3d = false, std::vector<int>* bounds = nullptr);

        /**
         * Read actuator limits from the system model
         *
         * The actuator limits are determined by the `effort="..."` clauses.
         * The lower and upper limits of each actuator will be listed. The number of actuators `size_u` is used.
         * Only the nast n joints will then be considered. If an actuator has no limits, then <-1.0e19, 1.0e19> is
         * filled in.
         * Pass a not-null pointer as an argument to also get a list of bounded actuators.
         *
         * @param[out] bounds Return a list of the actuators that are actually limited.
         * @return List of pairs representing limits
         */
        std::vector<std::pair<double, double>> GetActuatorLimits(std::vector<int>* bounds = nullptr);

    protected:

        /**
         * Propagate states through model
         *
         * Up until now the information is stored in the object properties,
         * without affecting the simulated world. So we update the world here.
         */
        void Update() override;

        /**
         * Get quaternion in matrix form such that:
         * 		d quat/dt 	= 0.5 quat x omega		(quaternion multiplication)
         * 					= 0.5 quat_M * omega		(matrix multiplication)
         *
         * This is \f$\tilde{Q_{\omega}}\f$ in the documentation.
         */
        MatrixXd QuaternionMatrix(const Vector4d& quat) const override;

        /**
         * Get angular velocity in matrix form such that:
         * 		d quat/dt 	= 0.5 quat x omega		(quaternion multiplication)
         * 					= 0.5 * omega_M * quat	(matrix multiplication)
         *
         * This is \f$\tilde{\Omega}\f$ in the documentation.
         */
        MatrixXd AngularVelocityMatrix(Vector3d omega) const override;

        /**
         * Get frame of end-effector
         *
         * @param ee_id End-effector id
         */
        const raisim::CoordinateFrame& GetEEFrame(uint ee_id) const;

        /**
         * Set actuator torque in system_
         */
        void SetSystemActuatorTorque(const VectorXd& u) const;

        /**
         * Set end-effector forces to the raisim system_
         *
         * @param forces    Set forces as a list of vectors
         */
        void SetSystemEEForces(const std::vector<VectorXd>& forces) const;

        /**
         * Raisim simulation world
         *
         * `mutable` because updating its information requires writing.
         */
        mutable raisim::World world_;

        /**
         * The actual URDF robot
         */
        mutable raisim::ArticulatedSystem* system_;

        /**
         * Ground object (needed to save for visualization)
         */
        mutable raisim::Ground* ground_;

        /**
         * URDF file used to create robot
         */
        std::string file_;

        /**
         * End-effector names (needed for clone method)
         */
        std::vector<std::string> ee_names_;

        /**
         * External forces on the system, mirror to ee_forces_
         *
         * A raisim system does not remember external forces. So instead we track our own list of forces that we
         * actually apply at the start of GetDynamics(). This is separate from ee_forces_ to make e.g. the dynamics
         * jacobian easier.
         */
        mutable std::vector<VectorXd> system_ee_forces_;

        /**
         * Track whether RaiSim activation has been performed at all
         */
        static bool activated;

        const double eps = 1e-5; ///< Finite-difference step

        /**
         * Joint actuator selection matrix
         *
         * Used like:
         * \f[
         *      \tau = S_a * \tau_a
         * \f]
         * There is no convenient way of relating joints and actuators, so this explicit matrix is used instead.
         * By default the selection matrix will skip the first 6 DOF if a floating joint is registered. In case more
         * joints are passive, overwrite this matrix.
         */
        Eigen::MatrixXd actuator_selector_;

        /**
         * List of frame ids corresponding to end-effectors
         */
        std::vector<size_t> ee_frame_ids_;

        /**
         * List of references to the frames of end-effectors
         *
         * Pointers actually, because a vector of references is not allowed.
         */
        std::vector<const raisim::CoordinateFrame*> ee_frames_;
    };

} /* namespace gambol */

#endif /* ROBOTS_RAISIMROBOTMODEL_H_ */

#ifndef ROBOTS_ROBOTMODEL_H_
#define ROBOTS_ROBOTMODEL_H_

/**
 * Set to `true` to normalize quaternions inside joint positions before writing to a model. When `false` no checks
 * are performed.
 *
 * Keeping this `false` should be fine most of the time. Small perturbations in a quaternion can be dealt with by the
 * simulator.
 * Note this does not directly modify the optimization variables, only the copied values. Also note note this cannot
 * replace a quaternion constraint.
 */
#ifndef NORMALIZE_QUATERNIONS
#define NORMALIZE_QUATERNIONS true
#endif

#include <memory>
#include <gambol/Variables/NodesHolder.h>

namespace gambol {

    /**
     * Class for robot instances that can be inserted into the optimizer
     *
     * Extend this class with your own robots. You could wrap an existing simulator or rely on analytical expressions.
     */
    class RobotModel {
    public:
        using Ptr = std::shared_ptr<RobotModel>;
        using VectorXd = Eigen::VectorXd;
        using Vector3d = Eigen::Vector3d;
        using Vector4d = Eigen::Vector4d;
        using MatrixXd = Eigen::MatrixXd;
        using Jacobian = Eigen::SparseMatrix<double, Eigen::RowMajor>;

        /**
         * Constructor
         */
        RobotModel(int size_q, int size_dq, int size_u, int ee_count);

        virtual ~RobotModel() = default;

        virtual RobotModel::Ptr clone() const = 0;

        /**
         * Whether IK is enabled
         *
         * Not necessary by default
         */
        virtual bool HasIK() const;

        /**
         * Set current states
         */
        void SetCurrent(const VectorXd& q = VectorXd(0),
                        const VectorXd& dq = VectorXd(0),
                        const VectorXd& u = VectorXd(0),
                        const std::vector<VectorXd>& ee_forces = {});

        /**
         * Set current states through nodesholder
         */
        virtual void SetCurrent(const NodesHolder& s, int k);

        /**
         * Set current states through nodesholder and time in seconds
         */
        virtual void SetCurrentTime(const NodesHolder& s, double t);

        /**
         * Return number of end-effectors
         */
        int GetEECount() const;

        /**
         * Return vector of quaternion indices
         */
        virtual const std::vector<int>& GetQuatIndices() const;

        /**
         * Return total mass of robot
         */
        virtual double GetTotalMass() const;

        /**
         * Return gravitational constant
         */
        virtual double GetGravity() const;

        /**
         * Get current forward dynamics
         */
        virtual VectorXd GetDynamics() const = 0;

        /**
         * Get current dynamics jacobian w.r.t. position
         */
        virtual Jacobian GetDynamicsJacobianWrtPos() const = 0;

        /**
         * Get current dynamics jacobian w.r.t. velocity
         */
        virtual Jacobian GetDynamicsJacobianWrtVel() const = 0;

        /**
         * Get current dynamics jacobian w.r.t. torque
         */
        virtual Jacobian GetDynamicsJacobianWrtTorque() const = 0;

        /**
         * Get current dynamics jacobian w.r.t. forces
         */
        virtual Jacobian GetDynamicsJacobianWrtForces(uint ee_id) const = 0;

        /**
         * Return joint rates (qpos_dot) from joint velocities (qvel)
         */
        virtual VectorXd GetRatesFromVel() const;

        /**
         * Get jacobian of position derivative (qpos_dot) from velocity (qvel), w.r.t. qpos
         *
         * \f[
         * 			\dot{q} = B(q) * v
         * 		\partial \dot{q} / \partial q = \partial (B(q) * v) / \partial q
         * \f]
         */
        virtual Jacobian GetRatesJacobianWrtPos() const;

        /**
         * Get jacobian of position derivative (qpos_dot) from velocity (qvel), w.r.t. qvel
         *
         * Returns the matrix mapping joint velocities to joint rates:
         *
         * \f[
         * 				\dot{q} = B(q) * v
         * 		\partial \dot{q} / \partial v = B(q)
         * \f]
         */
        virtual Jacobian GetRatesJacobianWrtVel() const;

        /**
         * Get jacobian of velocity (qvel) from joint rates (qpos_dot), w.r.t. qpos_dot
         *
         * Returns the matrix mapping joint rates to joint velocities:
         *
         * \f[
         * 				v = B^-1(q) * \dot{q}
         * 		\partial v / \partial \dot{q} = B^-1(q)
         * \f]
         */
        virtual Jacobian GetVelJacobianWrtRates() const;

        /**
         * Get position in world coordinates of end-effector
         */
        virtual Vector3d GetEEPos(uint ee_id) const = 0;

        /**
         * Get jacobian of end-effector position to joint positions
         */
        virtual Jacobian GetEEPosJacobian(uint ee_id) const = 0;

        /**
         * Return position of base in world coordinates
         */
        virtual Vector3d GetBasePos() const;

        /**
         * Get jacobian of base position w.r.t. joint positions
         */
        virtual Jacobian GetBasePosJacobian() const;

        /**
         * Get body pose through inverse kinematics
         */
        virtual VectorXd GetInverseKinematics(const Vector3d& base_pos,
                                              const Vector4d& base_rot,
                                              const std::vector<Vector3d>& ee_pos) const;

        /**
         * Propagate states through model
         */
        virtual void Update() = 0;

        /**
         * Turn dense matrix into a sparse one
         *
         * This keeps zero values! So no sparsity is used
         */
        static Jacobian DenseToSparse(Eigen::MatrixXd mat);

    protected:

        /**
         * @name Methods to do quaternion magic
         *
         * This is focused on quaternions and angular velocities in body-fixed
         * frame, for quaternions like [w, x, y, z].
         *
         * Note: this is applicable to body-reference Quaternions! for
         * quaternions in fixed-reference, the QuaternionMatrix definition
         * is different.
         */
        //@{
        /**
         * Get quaternion derivative from angular velocity
         */
        Vector4d QuaternionDerivative(const Vector4d& quat, const Vector3d& omega) const;

        /**
         * Get quaternion in matrix form such that:
         * 		d quat/dt 	= 0.5 quat x omega		(quaternion multiplication)
         * 					= 0.5 quat_M * omega		(matrix multiplication)
         *
         * This is \f$\tilde{Q_{\omega}}\f$ in the documentation.
         */
        virtual MatrixXd QuaternionMatrix(const Vector4d& quat) const;

        /**
         * Get quaternion in matrix form such that:
         * 		omega 	= 2 * dquat/dt x q^-1		(quaternion multiplication)
         * 				= 2 *  quat_M * dquat/dt	(matrix multiplication)
         *
         * This is \f$\tilde{Q_{\dot{Q}}}\f$ in the documentation.
         */
        MatrixXd QuaternionMatrixTranspose(const Vector4d& quat) const;

        /**
         * Get angular velocity in matrix form such that:
         * 		d quat/dt 	= 0.5 quat x omega		(quaternion multiplication)
         * 					= 0.5 * omega_M * quat	(matrix multiplication)
         *
         * This is \f$\tilde{\Omega}\f$ in the documentation.
         */
        virtual MatrixXd AngularVelocityMatrix(Vector3d omega) const;

        //@}

        int size_q_, size_dq_, size_u_;
        int ee_count_;

        VectorXd q_; ///< Generalized coordinates
        VectorXd dq_; ///< Generalized velocity
        VectorXd u_; ///< Joint torques (_not_ generalized force)

        std::vector<VectorXd> ee_forces_; ///< End-effector forces

        /**
         * Indices in gen. coordinates that correspond to the start of a quaternion
         *
         * Can simply be empty if there are no quaternions involved.
         */
        std::vector<int> quat_ids_;
    };

} /* namespace gambol */

#endif /* ROBOTS_ROBOTMODEL_H_ */

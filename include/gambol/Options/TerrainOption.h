#ifndef INCLUDE_GAMBOL_CONSTRAINTS_OPTIONFLATTERRAIN_H_
#define INCLUDE_GAMBOL_CONSTRAINTS_OPTIONFLATTERRAIN_H_

/**
 * When `TERRAIN_FLAT` is set to `true`, the terrain option won't be actually considered and instead
 * a flat terrain is presumed.
 * This is useful when no special terrain is used. Enabling TERRAIN_FLAT provides a much faster
 * optimization than a real terrain object (even if that object is just flat too).
 */

#ifndef TERRAIN_FLAT
#define TERRAIN_FLAT true
#endif

#endif //INCLUDE_GAMBOL_CONSTRAINTS_OPTIONFLATTERRAIN_H_

#ifndef GAMBOL_MODELS_BIPEDFEET_GAIT_GENERATOR_H_
#define GAMBOL_MODELS_BIPEDFEET_GAIT_GENERATOR_H_

#include "GaitGenerator.h"

namespace gambol {

/**
 * @brief Produces the contact sequence for a variety of two-legged gaits including heel and toe
 *
 * @sa GaitGenerator for more documentation
 */
    class GaitGeneratorBipedFeet : public GaitGenerator {
    public:
        GaitGeneratorBipedFeet();

        virtual ~GaitGeneratorBipedFeet() = default;

        enum BipedFeetIDs {
            // Left and right, heel and toe
            LH,
            LT,
            RH,
            RT
        };

    protected:
        GaitInfo GetGait(Gaits gait) const override;

        GaitInfo GetStrideStand() const;

        GaitInfo GetStrideFlight() const;

        GaitInfo GetStrideWalk() const;

        GaitInfo GetStrideWalkBegin() const;

        GaitInfo GetStrideWalkEnd() const;

        GaitInfo GetStrideWalkTiptoe() const;

        GaitInfo GetStrideWalkSneak() const;

        GaitInfo GetStrideRun() const;

        GaitInfo GetStrideHop() const;

        GaitInfo GetStrideLeftHop() const;

        GaitInfo GetStrideRightHop() const;

        GaitInfo GetStrideGallopHop() const;

        GaitInfo GetStrideBalance() const;

        void SetCombo(Combos combo) override;

        // naming convention:, where the circle is is contact, front is right ->.
        // so RF and LH in contact is (Pb):  o .
        //                                   . o
        // flight-phase
        ContactState II_;
        // 1 swingleg
        ContactState PI_;
        ContactState bI_;
        ContactState IP_;
        ContactState Ib_;
        // 2 swinglegs
        ContactState Pb_;
        ContactState bP_;
        ContactState BI_;
        ContactState IB_;
        ContactState PP_;
        ContactState bb_;
        // 3 swinglegs
        ContactState Bb_;
        ContactState BP_;
        ContactState bB_;
        ContactState PB_;
        // stance-phase
        ContactState BB_;
    };

} /* namespace gambol */

#endif /* GAMBOL_MODELS_BIPEDFEET_GAIT_GENERATOR_H_ */

#ifndef CONTROL_MPC_H_
#define CONTROL_MPC_H_

#include <gambol/Problem/NlpFormulation.h>
#include <ifopt/ipopt_solver.h>

namespace gambol {

    /**
     * Wrapper for model predictive control for simulation
     *
     * Functionality is fairly basic, main purpose is to wrap the
     * optimization update.
     */
    class MPC {
    public:
        using VectorXd = Eigen::VectorXd;

        /**
         * Constructor.
         */
        MPC();

        /**
         * Destructor.
         */
        virtual ~MPC() = default;

        /**
         * Set new initial configuration
         *
         * This is used to fix the MPC to the current state.
         */
        void SetInitialConfig(const VectorXd& qpos, const VectorXd& qvel);

        /**
         * Update time
         *
         * Bring phases closer by shortening the tip. End of phases is padded with contact (stance
         * mode) to achieve a rolling horizion.
         */
        void SetTime(double t);

        /**
         * Perform optimization on current configurations
         *
         * Function is blocking until optimization is done.
         *
         * @param solution		NodesHolder for optimized results
         * @return True if solver return is SUCCESS or ACCEPTABLE
         */
        bool Predict(NodesHolder& solution);

        NlpFormulation formulation_;
        ifopt::IpoptSolver::Ptr solver_;
        ifopt::Problem nlp_;
        int optimizations_;

    private:

        /**
         * Interpolate a new variable set from a previous solution.
         *
         * The solution from a previous optimization will have different node times. This method
         * will use the new times to interpolate the previous solution.
         */
        void InterpolateOldSolution(const ifopt::VariableSet::Ptr& set,
                                    const NodesHolder& old_solution);

        /**
         * Get a single variable from NodesHolder based on name.
         */
        static NodesVariables::Ptr GetVariableFromHolder(const NodesHolder& holder,
                                                         const std::string& name);

        double t_last_; ///< Simulation time of last update
        double delta_t_; ///< Simulation time difference between last updates
    };

} /* namespace gambol */

#endif /* CONTROL_MPC_H_ */

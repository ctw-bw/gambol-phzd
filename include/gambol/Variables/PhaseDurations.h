#ifndef VARIABLES_PHASEDURATIONS_H_
#define VARIABLES_PHASEDURATIONS_H_

#include <memory>
#include <vector>

namespace gambol {

    /**
     * Class for durations of contact phases.
     *
     * This class determined whether a node is part of stance or swing.
     * These durations are not optimized over, so they are not optimization variables.
     */
    class PhaseDurations {
    public:
        using Ptr = std::shared_ptr<PhaseDurations>;
        using VecDurations = std::vector<double>;

        /**
         * Constructor.
         *
         * @param timings 		List of phase timings
         * @param is_first_phase_in_contact 	`true` to start in contact
         */
        PhaseDurations(const VecDurations& initial_durations,
                       bool is_first_phase_in_contact);

        /**
         * Destructor.
         */
        virtual ~PhaseDurations() = default;

        /**
         * Return vector with all durations.
         */
        const VecDurations& GetPhaseDurations() const;

        /**
         * Check whether in contact at a certain time
         */
        bool IsContactPhase(double t) const;

    private:
        VecDurations durations_; ///< Actual durations
        double t_total_;
        bool initial_contact_state_; ///< True if first phase in contact
    };

} /* namespace gambol */

#endif /* VARIABLES_PHASEDURATIONS_H_ */

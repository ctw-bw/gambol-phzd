#ifndef VARIABLES_NODETIMES_H_
#define VARIABLES_NODETIMES_H_

#include <vector>
#include <memory>

namespace gambol {

    /**
     * Class for the times of each node (= collocation point)
     *
     * This class is not essential, but it makes it easier to share
     * a single instance of nodes times between different components.
     *
     * Note: NodeTimes are not actually optimized over, so they are not
     * optimization variables.
     */
    class NodeTimes {
    public:
        using Ptr = std::shared_ptr<NodeTimes>;
        using VecTimes = std::vector<double>;

        /**
         * Construct with entire list
         *
         * @param times		List of all node times
         */
        explicit NodeTimes(const VecTimes& times);

        /**
         * Construct with delta_t and number of nodes
         *
         * @param t_total	Total duration
         * @param n_nodes	Number of nodes
         */
        NodeTimes(double t_total, int n_nodes);

        /**
         * Destructor
         */
        virtual ~NodeTimes() = default;

        /**
         * Return number of nodes
         */
        int GetNumberOfNodes() const;

        /**
         * Get total duration
         */
        double GetTotalTime() const;

        /**
         * Get time of node k
         */
        double at(int k) const;

        /**
         * Get entire list
         */
        const VecTimes& GetList() const;

        /**
         * Return id of node right before given time.
         *
         * Return first node when t < 0 and last node when t > GetTotalTime().
         */
        int GetNodeId(double t_des) const;

        /**
         * Return id of node right before given time _and_ the interpolation factor
         *
         * Most right node is ignored.
         * Method helps to interpolate random objects.
         */
        int GetNodeId(double t_des, double& interpolation) const;

        /**
         * Get duration of this node and the next
         *
         * Throws error when index is out of range
         */
        double GetDeltaT(int k) const;

    private:
        VecTimes node_times_;
    };

} /* namespace gambol */

#endif /* VARIABLES_NODETIMES_H_ */

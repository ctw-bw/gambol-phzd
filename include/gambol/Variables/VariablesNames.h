#ifndef VARIABLES_VARIABLESNAMES_H_
#define VARIABLES_VARIABLESNAMES_H_

#include <string>

namespace gambol {

    /**
     * Provide constants to recognize the variable names
     */
    namespace id {

        static const std::string joint_pos = "joint-pos";
        static const std::string joint_vel = "joint-vel";
        static const std::string joint_acc = "joint-acc";
        static const std::string torques = "torques";
        static const std::string forces = "ee-forces_";
        static const std::string time = "time";
        static const std::string time_dt = "time-dt";
        static const std::string time_ddt = "time-ddt";
        static const std::string beziers = "beziers";

        std::string EEForceNodes(uint ee);

    } // namespace id

} // namespace gambol

#endif /* VARIABLES_VARIABLESNAMES_H_ */

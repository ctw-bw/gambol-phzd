#ifndef VARIABLES_NODESHOLDER_H_
#define VARIABLES_NODESHOLDER_H_

#include "NodesVariables.h"
#include "BezierVariables.h"
#include "NodeTimes.h"
#include "PhaseDurations.h"

namespace gambol {

    /**
     * Struct to group all pointers to important variables together.
     */
    struct NodesHolder {
        /**
         * Default constructor
         */
        NodesHolder() = default;

        /**
         * Real constructor
         */
        NodesHolder(const NodesVariables::Ptr& joint_pos,
                    const NodesVariables::Ptr& joint_vel,
                    const NodesVariables::Ptr& joint_acc,
                    const NodesVariables::Ptr& torques,
                    const std::vector<NodesVariables::Ptr>& ee_forces,
                    const NodesVariables::Ptr& time,
                    const NodesVariables::Ptr& time_dt,
                    const NodesVariables::Ptr& time_ddt,
                    const BezierVariables::Ptr& beziers,
                    const std::vector<PhaseDurations::Ptr>& phase_durations,
                    const NodeTimes::Ptr& nodes_times);

        /**
         * Destructor
         */
        virtual ~NodesHolder() = default;

        NodesVariables::Ptr joint_pos_, joint_vel_, joint_acc_;
        NodesVariables::Ptr torques_;
        std::vector<NodesVariables::Ptr> ee_forces_;
        NodesVariables::Ptr time_, time_dt_, time_ddt_;
        BezierVariables::Ptr beziers_;

        std::vector<PhaseDurations::Ptr> phase_durations_;
        NodeTimes::Ptr node_times_;
    };

} /* namespace gambol */

#endif /* VARIABLES_NODESHOLDER_H_ */

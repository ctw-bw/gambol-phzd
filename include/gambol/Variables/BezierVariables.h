#ifndef SRC_VARIABLES_BEZIERVARIABLES_H_
#define SRC_VARIABLES_BEZIERVARIABLES_H_

#include <ifopt/variable_set.h>

namespace gambol {

    /**
     * Variables that define a bezier spline for another variable.
     *
     * This parameterized spline is meant to map a variable (making it dependent) to another
     * (independent) variable. This mapping is invariant (i.e. not dependent on time).
     * Use one instance of this class for one variable vector.
     * The bezier variables make up some function:
     * \f[
     *      y = f(\alpha, \theta)
     *      \theta \in [0, 1]
     * \f]
     * Where theta is a timing variable.
     */
    class BezierVariables : public ifopt::VariableSet {

    public:

        using Ptr = std::shared_ptr<BezierVariables>;
        using VectorXd = Eigen::VectorXd;

        /**
         * Constructor.
         *
         * Note that a bezier curve of order N will have N + 1 coefficients
         *
         * @param dim Number of dimensions in the dependent variable
         * @param bezier_order Max. order of the curve (i.e. the complexity of the curve)
         */
        BezierVariables(int dim, int bezier_order, const std::string& name);

        /**
         * Destructor.
         */
        ~BezierVariables() override = default;

        /**
         * Get the function values for a given timing value and the current coefficients.
         *
         * @param s Normalized timing variable
         * @return Return vector of all bezier outputs
         */
        VectorXd GetCurveVector(double s) const;
        VectorXd GetCurveVectorDot(double s) const;
        VectorXd GetCurveVectorDotDot(double s) const;
        VectorXd GetCurveVectorDotDotDot(double s) const;

        /**
         * Get the function value for a given timing value and the current coefficients.
         *
         * @param dim Dimension to get value from
         * @param s Normalized timing variable
         * @return Return bezier output
         */
        double GetCurve(int dim, double s) const;

        /**
         * Get derivative of a single bezier curve w.r.t. to coefficients.
         *
         * Result is a row-vector. Each curve has the same gradient, as all coefficients have
         * dropped out in the derivation.
         */
        //Jacobian GetCoefficientJacobian(double s) const;

        /**
         * Get complete curves jacobian w.r.t. to coefficients.
         *
         * This jacobian is the derivative of the complete bezier vector
         * w.r.t. to the stacked vector of coefficients.
         * The matrix will have n_dims rows and (M+1) * n_dims columns.
         */
        Jacobian GetCoefficientJacobian(double s) const;
        Jacobian GetDotCoefficientJacobian(double s) const;
        Jacobian GetDotDotCoefficientJacobian(double s) const;

        /**
         * Get jacobian relating the coefficients of a single curve to the vector containing all coefficients.
         *
         * The resulting matrix will have M+1 rows and (M+1) * n_dims columns. It will be mostly zeros, with
         * one identity block.
         */
        Jacobian GetCurveJacobian(int dim) const;

        /**
         * Get the factorial of a number (surprisingly there is no built-in function)
         *
         * @param n
         * @return Factorial of n ("n!")
         */
        static int Factorial(int n);

        /**
         * Pure optimization variables to pass through to IPOPT
         */
        VectorXd GetValues() const override;

        /**
         * Sets coefficients from the optimization variables.
         * @param x The optimization variables.
         */
        void SetVariables(const VectorXd& x) override;

        /**
         * @returns The bounds of the coefficients
         */
        VecBound GetBounds() const override;

        /**
         * @return Const reference to list of coefficient vectors
         */
        const std::vector<VectorXd>& GetCoefficients() const;

    private:

        /**
         * Get reused segment.
         *
         * @return M! / (m! * (M - m)!) * s^m * (1 - s)^(M - m)
         */
        static double Beta(int M, int m, double s);

        int dim_; ///< Dimensions in dependent variable
        int M_; ///< Highest index of the coefficients (bezier order)
        int num_coeffs_; ///< NUmber of coefficients (M + 1)

        std::vector<VectorXd> coeffs_; ///< Bezier coefficients

        VecBound bounds_; ///< Bound of the coefficients

    };

}


#endif //SRC_VARIABLES_BEZIERVARIABLES_H_

#ifndef CONSTRAINTS_FORCECONSTRAINT_H_
#define CONSTRAINTS_FORCECONSTRAINT_H_

#include "NodesConstraint.h"

#include <gambol/Robots/RobotModel.h>
#include <gambol/Terrain/HeightMap.h>

namespace gambol {

    /**
     * Constraint for ground reaction forces.
     *
     * This constraint does two things:
     *  - Keep force zero during swing phase
     *  - Limit force to positive normal and pyramid friction cone
     *
     * More exactly:
     *
     *     0 <= dot(F, n) < F_normal_max
     *     dot(F, t1 - mu * n) < 0
     *     dot(F, t1 + mu * n) > 0
     *     dot(F, t2 - mu * n) < 0
     *     dot(F, t2 + mu * n) > 0
     *
     * Note: use `TERRAIN_FLAT` to force a flat terrain to speed up optimization.
     */
    class ForceConstraint : public NodesConstraint {
    public:
        ForceConstraint(const HeightMap::Ptr& terrain,
                        const RobotModel::Ptr& model,
                        const NodesHolder& nodes_holder, double max_normal, uint ee_id);

        /**
         * Constructor
         */
        ~ForceConstraint() override = default;

    private:

        /**
         * Get constraint values
         */
        void UpdateConstraintAtNode(int k, VectorXd& g) const override;

        /**
         * Get constraint bounds
         */
        void UpdateBoundsAtNode(int k, VecBound& b) const override;

        /**
         * Fill jacobian for constraint
         */
        void UpdateJacobianAtNode(int k, std::string var_set,
                                  Jacobian& jac) const override;

        /**
         * Get row of constraint vector based on node info
         */
        int GetRow(int k, int type = 0) const;

        /**
         * Update state properties for current node
         */
        void Update(int k) const;

        HeightMap::Ptr terrain_;
        mutable RobotModel::Ptr model_; // Model is need to find ee_pos

        uint ee_id_;
        double max_normal_force_; ///< Maximum normal force that's allowed
        double mu_; ///< Friction coefficient
    };

} /* namespace gambol */

#endif /* CONSTRAINTS_FORCECONSTRAINT_H_ */

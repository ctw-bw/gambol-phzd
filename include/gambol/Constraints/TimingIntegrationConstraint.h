#ifndef INCLUDE_GAMBOL_CONSTRAINTS_TIMINGINTEGRATIONCONSTRAINT_H_
#define INCLUDE_GAMBOL_CONSTRAINTS_TIMINGINTEGRATIONCONSTRAINT_H_

#include "NodesConstraint.h"


namespace gambol {

    /**
     * Constraint to integrate timing variable.
     *
     * This constraint was later implemented differently inside TimingConstraint!
     *
     * Constraint value is:
     *
     *      time[k] - time[k+1] + dt/2 * (time_dt[k] + time_dt[k+1])
     *      time_dt[k] - time_dt[k+1] + dt/2 * (time_ddt[k] + time_ddt[k+1])
     */
    class TimingIntegrationConstraint : public NodesConstraint {

    public:
        /**
         * Constructor
         */
        TimingIntegrationConstraint(const NodesHolder& nodes_holder);

        ~TimingIntegrationConstraint() override = default;

    private:
    private:

        /**
         * Get row of constraint vector based on node info
         *
         * @param k Node index [0, N - 1]
         * @param type Constraint type (0 = velocity, 1 = acceleration)
         */
        int GetRow(int k, int type = 0) const;

        /**
         * Update state properties for current node
         *
         * @return bool		False if k is the last node
         */
        bool Update(int k) const;

        /**
         * Update constraint value at node
         */
        void UpdateConstraintAtNode(int k, VectorXd& g) const override;

        /**
         * Set bound for specific node
         */
        void UpdateBoundsAtNode(int k, VecBound& b) const override;

        /**
         * Set jacobian for this node
         *
         * `jac` is the jacobian of the entire constraint to the current variable
         * set.
         */
        void UpdateJacobianAtNode(int k, std::string var_set,
                                  Jacobian& jac) const override;

        // `mutable` to ignore const flag
        mutable double dt_k_;

    };

}


#endif //INCLUDE_GAMBOL_CONSTRAINTS_TIMINGINTEGRATIONCONSTRAINT_H_

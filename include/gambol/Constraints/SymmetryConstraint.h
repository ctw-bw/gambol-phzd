#ifndef CONSTRAINTS_SYMMETRYCONSTRAINT_H_
#define CONSTRAINTS_SYMMETRYCONSTRAINT_H_

#include <ifopt/constraint_set.h>

#include <gambol/Variables/NodesVariables.h>

namespace gambol {

    /**
     * Constraint to keep the initial pose identical the last pose.
     *
     * This constraint is relevant for the first and last node. So extending `NodesConstraint` is not
     * convenient.
     *
     * Mirrored symmetry is also allowed (for e.g. single steps), so the constraint is defined at:
     *
     *      S_f * q[N] - S_0 * q[1] = 0
     */
    class SymmetryConstraint : public ifopt::ConstraintSet {
    public:
        using VectorXd = Eigen::VectorXd;
        using MatrixXd = Eigen::MatrixXd;
        using VecTimes = std::vector<double>;
        using Bounds = ifopt::Bounds;

        /**
         * Constructor
         *
         * @param nodes		Pointer to NodesVariables to constrain
         * @param dims		List of dimensions to constrain (constrain all
         * 					when left empty)
         */
        explicit SymmetryConstraint(const NodesVariables::Ptr& nodes, const std::vector<int>& dims = {});

        /**
         * Constructor
         *
         * @param nodes     Pointer to NodesVariables to constrain
         * @param S_f       Matrix to multiply q[N] with
         * @param S_0       Matrix to multiply q[1] with (defaults to identity)
         */
        SymmetryConstraint(const NodesVariables::Ptr& nodes,
                           const MatrixXd& S_f,
                           const MatrixXd& S_0 = MatrixXd(0,0));

        ~SymmetryConstraint() override = default;

        /**
         * Get constraint values
         */
        VectorXd GetValues() const override;

        /**
         * Get bounds of the constraint
         */
        VecBound GetBounds() const override;

        /**
         * Fill in jacobian for the constraint
         */
        void FillJacobianBlock(std::string var_set, Jacobian& jac) const override;

    private:

        void Initialize();

        NodesVariables::Ptr nodes_;

        MatrixXd S_f_, S_0_; ///< Symmetry matrices
        int n_dims_; ///< Number of rows in the symmetry matrices
        Jacobian  jac_f_, jac_0_; ///< Sparse counterparts
    };

} /* namespace gambol */

#endif /* CONSTRAINTS_SYMMETRYCONSTRAINT_H_ */

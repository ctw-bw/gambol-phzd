#ifndef INCLUDE_GAMBOL_CONSTRAINTS_TIMINGCONSTRAINT_H_
#define INCLUDE_GAMBOL_CONSTRAINTS_TIMINGCONSTRAINT_H_

#include "NodesConstraint.h"

namespace gambol {

    /**
     * Constraint to link timing variable to system states.
     *
     * The timing variable is linearly interpolated from the joint states:
     *
     *      s[k] = (J * q[k] - J * q[0]) / (J * q[N] - J * q[0])
     *
     * Here `J` is typically some simplified constant jacobian.
     *
     * The constraint value is then:
     *
     *      (J * (q[k] - q[0])) / (J * (q[N] - q[0])) - s[k] = 0
     */
    class TimingConstraint : public NodesConstraint {

    public:

        using MatirxXd = Eigen::MatrixXd;

        /**
         * Constructor.
         *
         * @param J Simplified jacobian
         * @param nodes_holder Collection of variables
         */
        TimingConstraint(const NodesHolder& nodes_holder, const MatrixXd& J);

        ~TimingConstraint() override = default;

    private:

        /**
         * Get row of constraint vector based on node info
         */
        int GetRow(int k, int type) const;

        /**
         * Update state properties for current node
         *
         * @return bool		False if k is the last node
         */
        void Update(int k) const;

        /**
         * Update constraint value at node
         */
        void UpdateConstraintAtNode(int k, VectorXd& g) const override;

        /**
         * Set bound for specific node
         */
        void UpdateBoundsAtNode(int k, VecBound& b) const override;

        /**
         * Set jacobian for this node
         *
         * `jac` is the jacobian of the entire constraint to the current variable
         * set.
         */
        void UpdateJacobianAtNode(int k, std::string var_set,
                                  Jacobian& jac) const override;

        int size_q_, size_dq_;

        mutable double s_k_; ///< Computed timing variable in node k

        MatrixXd J_; ///< Simplified jacobian (for position)
        mutable MatrixXd S_mat_k_; ///< S-matrix, such that ds/dt = S_mat_k_ * dq
        MatrixXd B_inv_; ///< Simplified map from \dot{q} to v: v = B_inv * \dot{q} - The base is simply skipped!
    };

}


#endif //INCLUDE_GAMBOL_CONSTRAINTS_TIMINGCONSTRAINT_H_

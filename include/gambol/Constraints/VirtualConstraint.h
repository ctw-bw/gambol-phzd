#ifndef INCLUDE_GAMBOL_CONSTRAINTS_VIRTUALCONSTRAINT_H_
#define INCLUDE_GAMBOL_CONSTRAINTS_VIRTUALCONSTRAINT_H_

#include "NodesConstraint.h"
#include <ifopt/constraint_set.h>
#include <gambol/Robots/RobotModel.h>

/**
 * The virtual constraints contain parts that are applied at every node and parts
 * that are applied once. But since they share much of the same logic, they are
 * kept as one class that does not extend NodesConstraint.
 */

namespace gambol {

    /**
     * The virtual constraint limits the joint positions to the timing variable.
     *
     * The selected degrees of freedom are virtually limited by a function of a timing variable. If
     * the constraint is satisfied, partial zero dynamics is achieved.
     * The exact constraint is:
     * \f[
     *      \dot{y_1[k]} + \epsilon y1[k] = 0
     *      \ddot{y_2[k]} + 2\epsilon \dot{y2[k]} + \epsilon^2 y2[k] = 0
     *      y_2[0] = 0
     *      \dot{y_2}[0] = 0
     * \f]
     *
     * Where
     * \f[
     *      y_1 = v_{hip,x} - v_d
     * \f]
     * (the difference between the hip velocity and some desired velocity)
     *
     * And
     * \f[
     *      y_2 = H q - {y_2}^d(q, \alpha)
     * \f]
     * With `H` as some selection matrix.
     * Only the actuated positions are included. By default, these are the last `size_u` elements
     * of `q`.
     */
    class VirtualConstraint : public NodesConstraint {

    public:

        using MatrixXd = Eigen::MatrixXd;

        /**
         * @param model Pointer to the robot model
         * @param nodes_holder Pointer to the collection of relevant variables
         * @param v_d Desired velocity of the hips
         * @param J Simplified jacobian to get the hips velocity from joint velocity
         * @param H The selection matrix - By default the last `size_u` elements of
         *          `q` will be used
         */
        VirtualConstraint(const NodesHolder& nodes_holder,
                          double v_d,
                          const MatrixXd& J,
                          MatrixXd H = MatrixXd(0,0));

        /**
         * Return vector of constraint values
         */
        VectorXd GetValues() const override;

        /**
         * Return vector of constraint bounds
         */
        VecBound GetBounds() const override;

        /**
         * Return complete constraint jacobian
         */
        void FillJacobianBlock(std::string var_set, Jacobian&) const override;

    private:

        /**
         * Get row of constraint vector based on node info
         *
         * @param k Node (value is ignored for the non-node constraints)
         * @param type Which constraint (0, 1, 2 or 3)
         * @param dim Dimension inside constrain (not used for type == 0)
         * @return Index of this constraint in the large constraint vector
         */
        int GetRow(int k, int type = 0, int dim = 0) const;

        /**
         * Update constraint value at node
         */
        void UpdateConstraintAtNode(int k, VectorXd& g) const override;

        /**
         * Set bound for specific node
         */
        void UpdateBoundsAtNode(int k, VecBound& b) const override;

        /**
         * Set jacobian for this node
         *
         * `jac` is the jacobian of the entire constraint to the current variable
         * set.
         */
        void UpdateJacobianAtNode(int k, std::string var_set,
                                  Jacobian& jac) const override;

        /**
         * Get the values of the extra non-node constraints
         */
        void UpdateExtraConstraint(VectorXd& g) const;

        /**
         * Get the bounds of the extra non-node constraints
         */
        void UpdateExtraBounds(VecBound& b) const;

        /**
         * Return extra constraint jacobian
         */
        virtual void UpdateExtraJacobian(std::string var_set, Jacobian& jac) const;

        int size_q_, size_dq_, size_u_;
        int size_y2_; ///< Number of coordinates constraint by y2
        double v_d_; ///< Desired hip velocity
        MatrixXd H_; ///< Selection matrix (position)
        MatrixXd J_; ///< Simplified jacobian (position)
        MatrixXd B_inv_; ///< Simplified map from \dot{q} to v: v = B_inv * \dot{q} - The base is simply skipped!
        double epsilon_; ///< Scaling factor for the terms in the constraint
    };

}


#endif //INCLUDE_GAMBOL_CONSTRAINTS_VIRTUALCONSTRAINT_H_

#include "Variables/NodesHolder.h"
#include "Problem/NlpFormulation.h"

int main();

/**
 * Toggle between generation (main.cpp) and simulation (simulate.cpp)
 */
#define MODE_GENERATE true

#define MODE_SIMULATE !MODE_GENERATE

using namespace gambol;

// Show nodes in NLP solution
void printSolution(const NodesHolder& solution);

// Show the constraints which are violated
void printConstraintViolations(ifopt::Problem& nlp);

// Show the contact schedule per end-effector
void printContactSchedule(const NodesHolder& solution);

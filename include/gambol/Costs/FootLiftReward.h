#ifndef COSTS_FOOTLIFTREWARD_H_
#define COSTS_FOOTLIFTREWARD_H_

#include <ifopt/cost_term.h>

#include <gambol/Robots/RobotModel.h>
#include <gambol/Variables/NodesHolder.h>

namespace gambol {

    /**
     * Reward (negative cost) for the height of a foot above the ground.
     */
    class FootLiftReward : public ifopt::CostTerm {
    public:
        using Vector3d = Eigen::Vector3d;

        /**
         * Constructor.
         *
         * @param node_times
         * @param phase_durations
         * @param model
         * @param ee_id			End-effector this applies to
         * @param weight		Weight should be positive to make reward
         */
        FootLiftReward(const NodeTimes::Ptr& node_times,
                       const std::vector<PhaseDurations::Ptr>& phase_durations,
                       const RobotModel::Ptr& model, uint ee_id, double weight);

        /**
         * Destructor.
         */
        ~FootLiftReward() override = default;

        /**
         * Link variables.
         */
        void InitVariableDependedQuantities(const VariablesPtr& x) override;

        /**
         * Get cost value.
         */
        double GetCost() const override;

    private:
        /**
         * Update the model with the current frame.
         *
         * @param k
         * @return bool		Return false if not using this node
         */
        bool Update(int k) const;

        /**
         * Fill in jacobian section.
         */
        void FillJacobianBlock(std::string var_set, Jacobian&) const override;

        mutable RobotModel::Ptr model_;
        NodeTimes::Ptr node_times_;
        NodesVariables::Ptr joint_pos_;
        std::vector<PhaseDurations::Ptr> phase_durations_;
        uint ee_id_;
        double weight_;
    };

} /* namespace gambol */

#endif /* COSTS_FOOTLIFTREWARD_H_ */

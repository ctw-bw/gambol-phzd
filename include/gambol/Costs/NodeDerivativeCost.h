#ifndef COSTS_NODEDERIVATIVECOST_H_
#define COSTS_NODEDERIVATIVECOST_H_

#include "NodeCost.h"

namespace gambol {

    /**
     * Cost based on the derivative of a NodesVariable.
     *
     * The derivative is calculated as the scaled difference between adjacent nodes.
     * So the final sum will have N-1 terms.
     */
    class NodeDerivativeCost : public NodeCost {
    public:

        /**
         * Constructor.
         *
         * @param nodes_id		Name of the linked variable
         * @param dim			Dimension of this variable to use
         * @param weight 		Relative weight of this cost
         */
        NodeDerivativeCost(const std::string& nodes_id, int dim, double weight);

        /**
         * Destructor.
         */
        ~NodeDerivativeCost() override = default;

        /**
         * Calculate cost value
         */
        double GetCost() const override;

        /**
         * Calculate section of jacobian
         */
        void FillJacobianBlock(std::string var_set, Jacobian& jac) const override;

    };

} /* namespace gambol */

#endif /* COSTS_NODEDERIVATIVECOST_H_ */

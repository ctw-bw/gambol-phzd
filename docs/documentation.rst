Documentation
=============


Add your own model
------------------

A model (= combination of dynamics model and formulation settings) needs a lot of definition in different places. To simplify the process, optimization models can be prepared in the :class:`Robot` class.

To create a new optimization model:

 1. Add a new enum value to `RobotName` in `Robots.h`. Then declare a new `make...` method inside `Robots.h`.
 2. Now add a clause in the constructor inside `Robots.cpp` for your robot and create the definition of your `make...` method. Tip: you can start of my copying the existing definition of a model that resembles your model the most.
 3. Create your own XML and URDF file and refer to it from your model definition.
 4. In `main.cpp`, change the `robot` variable to your own new type. Use the code that follows to make any additional tweaks.


Lock / unlock a joint
---------------------

To lock a joint in your model there are two approaches:

 1. Replace the joint by a fixed one.
      * This effectively removes the joint entirely. The DOF will be removed and you will need to adjust your joint positions and velocities accordingly.
 2. Fix the joint by limits.
      * You could also set the joint limits to one value. You will need to make three changes: i) the limits need to be set in the dynamics model (XML or URDF file), ii) the limits need to be set inside the problem formulation and iii) the joint torque must be limited to zero. All this ensures the limiting torque is provided passively, instead of through the actuators.

In RaiSim you can specify joint limits inside the URDF. Note that a limit like `lower="0" upper="0"` is considered as no limit at all. The same goes for `effort="0"` Instead, you can create a very small but non-zero limit.
To automatically load limits from a RaiSim URDF file into your formulation, you could use:

.. code-block:: c++

   pos_limits = raisim_model->GetJointLimits(true, &pos_bounds);
   torque_limits = raisim_model->GetActuatorLimits(&torque_bounds);



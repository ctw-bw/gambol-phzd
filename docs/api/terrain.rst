Terrain
=======

Terrain objects are used to describe the terrain over to optimize a gait.

HeightMap
---------

.. doxygenfile:: Terrain/HeightMap.h


HeightMap Examples
------------------

.. doxygenfile:: Terrain/HeightMapExamples.h
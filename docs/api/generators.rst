Generators
==========

Gait generators are used to create the contact phases needed for a gait.

GaitGenerator
-------------

.. doxygenfile:: Generators/GaitGenerator.h

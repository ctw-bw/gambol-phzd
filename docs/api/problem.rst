Problem Formulation
===================

The following classes make up the problem formulation.

Nlp Formulation
---------------

.. doxygenfile:: Problem/NlpFormulation.h


Parameters
----------

.. doxygenfile:: Problem/Parameters.h


Saving NLP to file
------------------

.. doxygenfile:: Problem/NLPToFile.h

Variables
=========

Variables in the NLP are created through the following classes.


Nodes Variables
---------------

.. doxygenfile:: Variables/NodesVariables.h


Node Times
-----------

.. doxygenfile:: Variables/NodeTimes.h


Phase Durations
---------------

.. doxygenfile:: Variables/PhaseDurations.h


Variable Names
--------------

.. doxygenfile:: Variables/VariablesNames.h


Nodes Holder
------------

.. doxygenfile:: Variables/NodesHolder.h


Coordinates
-----------

.. doxygenfile:: Variables/Coordinates.h

#include <gambol/Variables/NodesHolder.h>

namespace gambol {

    // Constructor
    NodesHolder::NodesHolder(const NodesVariables::Ptr& joint_pos,
                             const NodesVariables::Ptr& joint_vel,
                             const NodesVariables::Ptr& joint_acc,
                             const NodesVariables::Ptr& torques,
                             const std::vector<NodesVariables::Ptr>& ee_forces,
                             const NodesVariables::Ptr& time,
                             const NodesVariables::Ptr& time_dt,
                             const NodesVariables::Ptr& time_ddt,
                             const BezierVariables::Ptr& beziers,
                             const std::vector<PhaseDurations::Ptr>& phase_durations,
                             const NodeTimes::Ptr& nodes_times) {
        joint_pos_ = joint_pos;
        joint_vel_ = joint_vel;
        joint_acc_ = joint_acc;
        torques_ = torques;
        ee_forces_ = ee_forces;
        time_ = time;
        time_dt_ = time_dt;
        time_ddt_ = time_ddt;
        beziers_ = beziers;
        phase_durations_ = phase_durations;
        node_times_ = nodes_times;
    }

} /* namespace gambol */

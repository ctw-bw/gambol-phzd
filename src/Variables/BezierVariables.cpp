#include <gambol/Variables/BezierVariables.h>

namespace gambol {

    // Constructor
    BezierVariables::BezierVariables(int dim, int bezier_order, const std::string& name) :
        ifopt::VariableSet(dim * (bezier_order + 1), name) {

        dim_ = dim;
        M_ = bezier_order;
        num_coeffs_ = M_ + 1;

        // Initialize coefficients to the right sizes
        for (int i = 0; i < dim_; i++) {
            coeffs_.emplace_back(VectorXd::Zero(num_coeffs_));
        }

        bounds_ = VecBound(GetRows(), ifopt::NoBound); // Initialize unbounded
    }

    // Get all bezier outputs
    Eigen::VectorXd BezierVariables::GetCurveVector(double s) const {
        VectorXd y(dim_);

        for (int i = 0; i < dim_; i++) {
            y[i] = GetCurve(i, s);
        }

        return y;
    }

    // Get all bezier first derivatives
    Eigen::VectorXd BezierVariables::GetCurveVectorDot(double s) const {
        VectorXd dy(dim_);

        for (int i = 0; i < dim_; i++) {

            double dy_i = 0.0;
            const auto& coeffs = coeffs_[i];

            for (int m = 0; m <= M_ - 1; m++) {
                dy_i += Beta(M_ - 1, m, s) * (coeffs[m + 1] - coeffs[m]);
            }

            dy[i] = (double)M_ * dy_i;
        }

        return dy;
    }

    // Get all bezier second derivatives
    Eigen::VectorXd BezierVariables::GetCurveVectorDotDot(double s) const {
        VectorXd ddy(dim_);

        for (int i = 0; i < dim_; i++) {

            double ddy_i = 0.0;
            const auto& coeffs = coeffs_[i];

            for (int m = 0; m <= M_ - 2; m++) {
                ddy_i += Beta(M_ - 2, m, s) * (coeffs[m + 2] - 2.0 * coeffs[m + 1] + coeffs[m]);
            }

            ddy[i] = (double)M_ * (M_ - 1) * ddy_i;
        }

        return ddy;
    }

    // Get all bezier third derivatives
    Eigen::VectorXd BezierVariables::GetCurveVectorDotDotDot(double s) const {
        VectorXd dddy(dim_);

        for (int i = 0; i < dim_; i++) {

            double ddy_i = 0.0;
            const auto& coeffs = coeffs_[i];

            for (int m = 0; m <= M_ - 3; m++) {
                ddy_i += Beta(M_ - 3, m, s) * (-coeffs[m] + 3.0 * coeffs[m + 1]
                        - 3.0 * coeffs[m + 2] + coeffs[m + 3]);
            }

            dddy[i] = (double)M_ * (M_ - 1) * (M_ - 2) * ddy_i;
        }

        return dddy;
    }

    // Get single bezier output
    double BezierVariables::GetCurve(int dim, double s) const {
        double y = 0;

        const auto& coeff = coeffs_.at(dim);

        for (int m = 0; m <= M_; m++) {

            y += coeff[m] * Beta(M_, m, s);
        }

        return y;
    }

    // Get coefficients jacobian
    /*BezierVariables::Jacobian BezierVariables::GetCoefficientJacobian(double s) const {

        Jacobian jac(1, num_coeffs_);

        for (int m = 0; m <= M_; m++) {
            jac.insert(1, m) = Beta(M_, m, s);
        }

        return jac;
    }*/

    // Get coefficients jacobian
    BezierVariables::Jacobian BezierVariables::GetCoefficientJacobian(double s) const {

        Jacobian jac(dim_, GetRows());

        // Fill a stair pattern in the jacobian

        // for each \partial b / \partial alpha_i
        for (int i = 0; i < dim_; i++) {
            // for each \partial b_i / \partial alpha_i,m
            for (int m = 0; m <= M_; m++) {
                int col = i * num_coeffs_ + m;
                jac.insert(i, col) = Beta(M_, m, s);
            }
        }

        return jac;
    }

    // Get coefficients jacobian of the time derivative
    BezierVariables::Jacobian BezierVariables::GetDotCoefficientJacobian(double s) const {

        Jacobian jac(dim_, GetRows());

        // Fill a stair pattern in the jacobian

        // for each \partial b' / \partial alpha_i
        for (int i = 0; i < dim_; i++) {
            // for each \partial b'_i / \partial alpha_i,m
            for (int m = 0; m <= M_; m++) {

                double db;

                if (m == 0) {
                    db = -Beta(M_ - 1, 0, s);
                } else if (m == M_) {
                    db = Beta(M_ - 1, M_ - 1, s);
                } else {
                    db = Beta(M_ - 1, m - 1, s) - Beta(M_ - 1, m, s);
                }

                int col = i * num_coeffs_ + m;
                jac.insert(i, col) = M_ * db; // Factor is the same for all
            }
        }

        return jac;
    }

    // Get coefficients jacobian of the second time derivative
    BezierVariables::Jacobian BezierVariables::GetDotDotCoefficientJacobian(double s) const {

        Jacobian jac(dim_, GetRows());

        // Fill a stair pattern in the jacobian

        // for each \partial b' / \partial alpha_i
        for (int i = 0; i < dim_; i++) {
            // for each \partial b'_i / \partial alpha_i,m
            for (int m = 0; m <= M_; m++) {

                double db;

                if (m == 0) {
                    db = Beta(M_ - 2, 0, s);
                } else if (m == 1) {
                    db = -2.0 * Beta(M_ - 2, 0, s) + Beta(M_ - 2, 1, s);
                } else if (m == M_ - 1) {
                    db = Beta(M_ - 2, M_ - 3, s) - 2.0 * Beta(M_ - 2, M_ - 2, s);
                } else if (m == M_) {
                    db = Beta(M_ - 2, M_ - 2, s);
                } else {
                    db = Beta(M_ - 2, m - 2, s) - 2.0 * Beta(M_ - 2, m - 1, s) + Beta(M_ - 2, m , s);
                }

                int col = i * num_coeffs_ + m;
                jac.insert(i, col) = M_ * (M_ - 1) * db; // Put factor here, same for all
            }
        }

        return jac;
    }

    // Get jacobian mapping a single curve to the full vector
    BezierVariables::Jacobian BezierVariables::GetCurveJacobian(int dim) const {
        Jacobian jac(num_coeffs_, GetRows());

        int col = dim * num_coeffs_; // First column of the identity block

        for (int i = 0; i < num_coeffs_; i++) {
            jac.insert(i, col + i) = 1.0;
        }

        return jac;
    }

    // Compute factorial
    int BezierVariables::Factorial(int n) {
        if (n <= 1) {
            return 1; // By definition
        }

        assert(n < 13 && "Factorial of more than 12 does not fit in integer");

        int prod = 1;
        for (int i = 2; i <= n; i++) {
            prod *= i;
        }

        return prod;
    }

    // Get Beta segment
    double BezierVariables::Beta(int M, int m, double s) {
        double binomial = (double)Factorial(M) / ((double)Factorial(m) * (double)(Factorial(M - m)));
        return binomial * pow(s, m) * pow((1 - s), (M - m));
        // Note that in C++, pow(0.0, 0.0) == 1.0, which is the convention needed for the bezier curves
    }

    // Pass variables to IPOPT
    Eigen::VectorXd BezierVariables::GetValues() const {
        VectorXd x(GetRows());

        // Copy list of vectors into a single vector
        int i = 0;
        for (const auto& coeff : coeffs_) {
            x.segment(i, num_coeffs_) = coeff;
            i += num_coeffs_;
        }

        return x;
    }

    // Set variables from IPOPT
    void BezierVariables::SetVariables(const VectorXd& x) {

        // Copy segments of the vector into the list of vectors
        int i = 0;
        for (VectorXd& coeff : coeffs_) {
            coeff = x.segment(i, num_coeffs_);
            i += num_coeffs_;
        }
    }

    // Get variable bounds for IFOPT
    BezierVariables::VecBound BezierVariables::GetBounds() const {
        return bounds_;
    }

    // Get list of coefficients
    const std::vector<Eigen::VectorXd>& BezierVariables::GetCoefficients() const {
        return coeffs_;
    }

}

#include <gambol/Control/CsvLogger.h>

#include <ctime>
#include <iomanip>
#include <iostream>

namespace gambol {

    // Constructor
    CsvLogger::CsvLogger(const std::string& name) {
        auto t = std::time(nullptr);
        auto time_start = std::localtime(&t);
        std::string file_name = "results/log_";
        if (!name.empty()) {
            file_name += name + "_";
        }
        std::ostringstream time_str;
        time_str << std::put_time(time_start, "%Y-%m-%d_%H%M");
        file_name += time_str.str() + ".csv";

        rows_ = 0;

        try {
            log_file_.open(file_name);
        }
        catch (std::ios_base::failure& e) {
            std::cerr << "Error opening file: " << name << std::endl
                      << e.what() << std::endl;
        }
    }

    // Log a new line
    void CsvLogger::log(double t, const VectorXd& qpos, const VectorXd& qvel,
                        const VectorXd& qacc, const VectorXd& qpos_r, const VectorXd& qvel_r,
                        const VectorXd& u_ff, const VectorXd& u_fb,
                        const std::vector<VectorXd>& forces,
                        const std::vector<VectorXd>& forces_r, int predict_count) {
        // Create header
        if (rows_ == 0) {
            log_file_ << "t,";
            for (int i = 0; i < qpos.size(); i++)
                log_file_ << "qpos_" << i << ",";
            for (int i = 0; i < qvel.size(); i++)
                log_file_ << "qvel_" << i << ",";
            for (int i = 0; i < qacc.size(); i++)
                log_file_ << "qacc_" << i << ",";
            for (int i = 0; i < qpos_r.size(); i++)
                log_file_ << "qpos_ref_" << i << ",";
            for (int i = 0; i < qvel_r.size(); i++)
                log_file_ << "qvel_ref_" << i << ",";
            for (int i = 0; i < u_ff.size(); i++)
                log_file_ << "u_ff_" << i << ",";
            for (int i = 0; i < u_fb.size(); i++)
                log_file_ << "u_fb_" << i << ",";
            for (unsigned int ee = 0; ee < forces.size(); ee++)
                for (int i = 0; i < 3; i++)
                    log_file_ << "forces_" << ee << "_" << i << ",";
            for (unsigned int ee = 0; ee < forces_r.size(); ee++)
                for (int i = 0; i < 3; i++)
                    log_file_ << "forces_ref_" << ee << "_" << i << ",";
            log_file_ << "mpc_count" << std::endl;
        }

        log_file_ << t << ",";

        for (const VectorXd* var : {&qpos, &qvel, &qacc, &qpos_r, &qvel_r, &u_ff, &u_fb}) {

            for (int i = 0; i < var->size(); i++) {
                log_file_ << &var[i] << ",";
            }
        }

        for (const auto& force : forces)
            for (int i = 0; i < 3; i++)
                log_file_ << force[i] << ",";

        for (const auto& force_r : forces_r)
            for (int i = 0; i < 3; i++)
                log_file_ << force_r[i] << ",";

        log_file_ << predict_count << std::endl;

        rows_++;
    }

    // Get file name from a complete path
    std::string CsvLogger::fileNameFromPath(std::string path) {
        // Remove directory if present
        const size_t last_slash_idx = path.find_last_of("\\/");
        if (last_slash_idx != std::string::npos) {
            path.erase(0, last_slash_idx + 1);
        }

        // Remove extension if present
        const size_t period_idx = path.rfind('.');
        if (period_idx != std::string::npos) {
            path.erase(period_idx);
        }

        return path;
    }

} /* namespace gambol */

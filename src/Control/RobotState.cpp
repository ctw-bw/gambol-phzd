#include <gambol/Control/RobotState.h>

namespace gambol {

    // Constructor
    RobotState::RobotState(const VectorXd& _qpos, const VectorXd& _qvel,
                           const VectorXd& _u_ff, const VectorXd& _u_fb) {
        qpos = _qpos;
        qvel = _qvel;
        u_ff = _u_ff;
        u_fb = _u_fb;
    }

} /* namespace gambol */

#include <gambol/Control/MPC.h>

#include <coin/IpReturnCodes.h>
#include <gambol/Problem/NLPToFile.h>
#include <gambol/Variables/NodesHolder.h>
#include <iostream>

namespace gambol {

    // Constructor
    MPC::MPC() {
        t_last_ = 0.0;
        delta_t_ = 0.0;
        optimizations_ = 0;

        // Create solver and set settings

        solver_ = std::make_shared<ifopt::IpoptSolver>();

        NlpFormulation::setSolverOptions(solver_);
        solver_->SetOption("max_cpu_time", 120.0);
        solver_->SetOption("max_iter", 500);
        solver_->SetOption("print_frequency_iter", 10);
        //solver_->SetOption("print_timing_statistics", "yes");
    }

    // Set new config
    void MPC::SetInitialConfig(const VectorXd& qpos, const VectorXd& qvel) {
        formulation_.initial_joint_pos_ = qpos;
        formulation_.initial_joint_vel_ = qvel;

        // Need exact match with last position
        //formulation_.bound_initial_joint_pos_ = Parameters::GetLinspaceVector(formulation_.size_q_);
        //formulation_.bound_initial_joint_vel_ = Parameters::GetLinspaceVector(formulation_.size_dq_);
    }

    // Update time
    void MPC::SetTime(double t) {
        double delta_t = t - t_last_;

        if (formulation_.params_.t_total > delta_t) {
            uint ee = 0;
            for (std::vector<double>& durations : formulation_.params_.ee_phase_durations_) {
                // For each EE, subtract time or remove phases entirely at the tip
                double delta_t_remaining = delta_t;
                while (delta_t_remaining > 1e-6) {
                    if (durations.front() >= delta_t_remaining) {
                        durations.front() -= delta_t_remaining;
                        break; // Done
                    }

                    delta_t_remaining -= durations.front(); // Time removed
                    durations.erase(durations.begin()); // Remove first phase
                    // Toggle initial phase type (since there is a new front)
                    formulation_.params_.initial_contact_[ee] =
                            !formulation_.params_.initial_contact_[ee];
                }

                // Pad ending such that horizon does not become too short
                if (formulation_.params_.t_total < 0.5) {
                    // Add removed time back at the end
                    durations.back() += delta_t;
                }

                ee++;
            }
            formulation_.params_.t_total = formulation_.params_.GetTotalTime();
        }

        t_last_ = t;
        delta_t_ = delta_t;
    }

    // Do a new prediction
    bool MPC::Predict(NodesHolder& solution) {
        NodesHolder old_solution = solution;

        // Construct problem and solve it
        solution = NodesHolder(); // Reset it, just to make sure

        NLPToFile nlp_file(&nlp_, "nlp_result.tmp");

        if (optimizations_ == 0) {
            // Only load and save the first prediction
            //nlp_file.SetGuessFromFile();
        } else {
            if (optimizations_ == 1) {
                //solver_->SetOption("max_iter", 0);
                formulation_.initial_zpos_ = true; // Use initial z-pos as fixed
                formulation_.bound_initial_joint_pos_ =
                        Parameters::GetLinspaceVector(formulation_.size_q_ - 1);
            }
        }

        nlp_ = ifopt::Problem(); // Reset

        for (auto& set : formulation_.GetVariableSets(solution)) {
            InterpolateOldSolution(set, old_solution);

            nlp_.AddVariableSet(set);
        }
        for (auto& con : formulation_.GetConstraints(solution)) {
            nlp_.AddConstraintSet(con);
        }
        for (auto& cost : formulation_.GetCosts()) {
            nlp_.AddCostSet(cost);
        }

        auto var = nlp_.GetOptVariables();

        // Solve problem! This will take a while...
        solver_->Solve(nlp_);

        using namespace Ipopt;

        int status = solver_->GetReturnStatus();
        bool solved = (status == Solve_Succeeded
                       || status == Solved_To_Acceptable_Level);

        if (solved && optimizations_ == 0) {
            //nlp_file.WriteFileFromResult();
        }

        optimizations_++;

        return solved;
    }

    // Interpolate from the old solution
    void MPC::InterpolateOldSolution(const ifopt::VariableSet::Ptr& set,
                                     const NodesHolder& old_solution) {
        if (!old_solution.joint_pos_) {
            // Old solution is empty, skip
            return;
        }

        NodesVariables::Ptr old_set = GetVariableFromHolder(old_solution,
                                                            set->GetName());

        VectorXd values(old_set->GetDim() * old_set->GetNumNodes());
        int i = 0;

        // Node times of new solution
        for (double time : formulation_.node_times_->GetList()) {
            VectorXd node = old_set->GetPoint(time + delta_t_);
            values.segment(i, node.size()) = node;
            i += (int)node.size();
        }

        set->SetVariables(values);
    }

    // Get single variable from struct
    NodesVariables::Ptr MPC::GetVariableFromHolder(const NodesHolder& holder,
                                                   const std::string& name) {
        if (name == holder.joint_pos_->GetName())
            return holder.joint_pos_;
        if (name == holder.joint_vel_->GetName())
            return holder.joint_vel_;
        if (name == holder.torques_->GetName())
            return holder.torques_;

        for (auto var : holder.ee_forces_) {
            if (name == var->GetName())
                return var;
        }

        assert(false && "Variable not found");
    }

}
/* namespace gambol */

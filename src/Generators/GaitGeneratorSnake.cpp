#include <gambol/Generators/GaitGeneratorSnake.h>

#include <cassert>
#include <iostream>

namespace gambol {

    GaitGeneratorSnake::GaitGeneratorSnake() {
        xxx_ = ContactState(3, false); // Flight
        xxo_ = xox_ = oxx_ = xoo_ = oxo_ = oox_ = ooo_ = xxx_;

        // One contact
        oxx_ = {true, false, false};
        xox_ = {false, true, false};
        xxo_ = {false, false, true};

        // Two contacts
        xoo_ = {false, true, true};
        oxo_ = {true, false, true};
        oox_ = {true, true, false};

        // Three contacts
        ooo_ = {true, true, true};

        SetGaits({Stand});
    }

    void GaitGeneratorSnake::SetCombo(Combos combo) {
        switch (combo) {
            case C0:
                SetGaits({Stand, Walk1, Stand});
                break;
            case C1:
                SetGaits({Stand});
                break;
            case C2:
                SetGaits({Stand});
                break;
            case C3:
                SetGaits({Stand});
                break;
            case C4:
                SetGaits({Stand});
                break;
            default:
                assert(false);
                std::cout << "Gait not defined\n";
                break;
        }
    }

    GaitGeneratorSnake::GaitInfo GaitGeneratorSnake::GetGait(Gaits gait) const {
        switch (gait) {
            case Stand:
                return GetStrideStand();
            case Flight:
                return GetStrideFlight();
            case Walk1:
                return GetStrideCrawl();
            case Walk2:
                return GetStrideCrawlMiddle();
            case Hop1:
                return GetStrideHop();
                /*case Walk2:
                    return GetStrideWalk();
                case Run1:
                    return GetStrideRun();
                case Run3:
                    return GetStrideRun();
                case Hop1:
                    return GetStrideHop();
                case Hop2:
                    return GetStrideLeftHop();
                case Hop3:
                    return GetStrideRightHop();
                case Hop5:
                    return GetStrideGallopHop();*/
            default:
                assert(false); // gait not implemented
        }

        return GaitInfo();
    }

    GaitGeneratorSnake::GaitInfo GaitGeneratorSnake::GetStrideStand() const {
        auto times = {0.2};
        auto contacts = {ooo_,};

        return std::make_pair(times, contacts);
    }

    GaitGeneratorSnake::GaitInfo GaitGeneratorSnake::GetStrideFlight() const {
        auto times = {0.5};
        auto contacts = {xxx_,};

        return std::make_pair(times, contacts);
    }

    GaitGeneratorSnake::GaitInfo GaitGeneratorSnake::GetStrideCrawl() const {
        double lift = 0.2;
        double pause = 0.05;
        auto times = {lift, pause, lift, pause, lift};
        auto phase_contacts = {oox_, ooo_, oxo_, ooo_, xoo_};

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorSnake::GaitInfo GaitGeneratorSnake::GetStrideCrawlMiddle() const {
        double lift = 0.2;
        double pause = 0.05;
        auto times = {lift, pause, lift};
        auto phase_contacts = {oxo_, ooo_, xox_};

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorSnake::GaitInfo GaitGeneratorSnake::GetStrideHop() const {
        double launch = 0.15;
        double flight = 0.10;
        double land = 0.15;
        auto times = {launch, flight, land};
        auto phase_contacts = {oxo_, xxx_, xox_};

        return std::make_pair(times, phase_contacts);
    }

} /* namespace gambol */

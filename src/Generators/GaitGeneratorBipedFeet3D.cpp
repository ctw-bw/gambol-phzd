#include <gambol/Generators/GaitGeneratorBipedFeet3D.h>

namespace gambol {

    GaitGeneratorBipedFeet3D::GaitGeneratorBipedFeet3D() {
        /**
         * There are 6 end-effectors, one in each heel and two in the
         * toes. For simplicity we will consider the two in the toes as
         * congruent.
         *
         * The sequence of end-effectors is:
         * 	left_heel, left_toes_l, left_toes_r, right_heel,
         * 	right_toes_r, right_toes_l
         */
        int n_ee = 6;
        ContactState init(n_ee, false);

        II_ = init; // flight_phase
        PI_ = bI_ = IP_ = Ib_ = init; // single contact
        Pb_ = bP_ = BI_ = IB_ = PP_ = bb_ = init; // two point support
        Bb_ = BP_ = bB_ = PB_ = init; // three point support
        BB_ = init; // flat support phase

        // flight_phase
        II_ = ContactState(n_ee, false);

        // one stance point
        PI_.at(LH) = true;
        bI_.at(RH) = true;
        IP_.at(LTO) = IP_.at(LTI) = true;
        Ib_.at(RTO) = Ib_.at(RTI) = true;

        // two stance points
        Pb_.at(LH) = true;
        Pb_.at(RTO) = Pb_.at(RTI) = true;

        bP_.at(RH) = true;
        bP_.at(LTO) = bP_.at(LTI) = true;

        BI_.at(LH) = true;
        BI_.at(RH) = true;

        IB_.at(LTO) = IB_.at(LTI) = true;
        IB_.at(RTO) = IB_.at(RTI) = true;

        PP_.at(LH) = true;
        PP_.at(LTO) = PP_.at(LTI) = true;

        bb_.at(RH) = true;
        bb_.at(RTO) = bb_.at(RTI) = true;

        // three stance points
        Bb_.at(LH) = true;
        Bb_.at(RH) = true;

        Bb_.at(RTO) = Bb_.at(RTI) = true;
        BP_.at(LH) = true;

        BP_.at(RH) = true;
        BP_.at(LTO) = BP_.at(LTI) = true;

        bB_.at(RH) = true;
        bB_.at(LTO) = bB_.at(LTI) = true;

        bB_.at(RTO) = bB_.at(RTI) = true;
        PB_.at(LH) = true;

        PB_.at(LTO) = PB_.at(LTI) = true;
        PB_.at(RTO) = PB_.at(RTI) = true;

        // four stance points
        BB_ = ContactState(n_ee, true);

        // default gait
        SetGaits({Stand});

    }

} /* namespace gambol */

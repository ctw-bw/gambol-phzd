#include <gambol/Generators/GaitGeneratorBipedFeet.h>

#include <cassert>
#include <iostream>

namespace gambol {

    GaitGeneratorBipedFeet::GaitGeneratorBipedFeet() {
        int n_ee = 4;
        ContactState init(n_ee, false);

        II_ = init; // flight_phase
        PI_ = bI_ = IP_ = Ib_ = init; // single contact
        Pb_ = bP_ = BI_ = IB_ = PP_ = bb_ = init; // two point support
        Bb_ = BP_ = bB_ = PB_ = init; // three point support
        BB_ = init; // flat support phase

        // flight_phase
        II_ = ContactState(n_ee, false);
        // one stance point
        PI_.at(LH) = true;
        bI_.at(RH) = true;
        IP_.at(LT) = true;
        Ib_.at(RT) = true;
        // two stance points
        Pb_.at(LH) = true;
        Pb_.at(RT) = true;
        bP_.at(RH) = true;
        bP_.at(LT) = true;
        BI_.at(LH) = true;
        BI_.at(RH) = true;
        IB_.at(LT) = true;
        IB_.at(RT) = true;
        PP_.at(LH) = true;
        PP_.at(LT) = true;
        bb_.at(RH) = true;
        bb_.at(RT) = true;
        // three stance points
        Bb_.at(LH) = true;
        Bb_.at(RH) = true;
        Bb_.at(RT) = true;
        BP_.at(LH) = true;
        BP_.at(RH) = true;
        BP_.at(LT) = true;
        bB_.at(RH) = true;
        bB_.at(LT) = true;
        bB_.at(RT) = true;
        PB_.at(LH) = true;
        PB_.at(LT) = true;
        PB_.at(RT) = true;
        // four stance points
        BB_ = ContactState(n_ee, true);

        // default gait
        SetGaits({Stand});
    }

    void GaitGeneratorBipedFeet::SetCombo(Combos combo) {
        switch (combo) {
            case C0:
                SetGaits({Stand, Walk1, Walk1, Stand});
                break;
            case C1:
                SetGaits({Stand, Run1, Run1, Stand});
                break;
            case C2:
                SetGaits({Stand, Hop1, Hop1, Stand});
                break;
            case C3:
                SetGaits({Stand, Hop1, Hop2, Stand});
                break;
            case C4:
                SetGaits({Stand, Hop5, Hop5, Stand});
                break;
            default:
                std::cout << "Gait not defined\n";
                assert(false);
                break;
        }
    }

    GaitGeneratorBipedFeet::GaitInfo GaitGeneratorBipedFeet::GetGait(
            Gaits gait) const {
        switch (gait) {
            case Stand:
                return GetStrideStand();
            case Flight:
                return GetStrideFlight();
            case Walk1:
                return GetStrideWalk();
            case Walk1B:
                return GetStrideWalkBegin();
            case Walk1E:
                return GetStrideWalkEnd();
            case Walk2:
                return GetStrideWalkTiptoe();
            case Walk3:
                return GetStrideWalkSneak();
            case Run1:
                return GetStrideRun();
            case Run3:
                return GetStrideRun();
            case Hop1:
                return GetStrideHop();
            case Hop2:
                return GetStrideLeftHop();
            case Hop3:
                return GetStrideRightHop();
            case Hop5:
                return GetStrideGallopHop();
            case Balance:
                return GetStrideBalance();
            default:
                assert(false); // gait not implemented
        }

        return GaitInfo();
    }

    GaitGeneratorBipedFeet::GaitInfo GaitGeneratorBipedFeet::GetStrideStand() const {
        auto times = {0.2,};
        auto contacts = {BB_,};

        return std::make_pair(times, contacts);
    }

    GaitGeneratorBipedFeet::GaitInfo GaitGeneratorBipedFeet::GetStrideFlight() const {
        auto times = {0.5,};
        auto contacts = {II_,};

        return std::make_pair(times, contacts);
    }

    /**
     * Regular relaxed walk
     *
     * See https://www.utdallas.edu/atec/midori/Handouts/walkingGraphs_files/gait.JPG
     */
    GaitGeneratorBipedFeet::GaitInfo GaitGeneratorBipedFeet::GetStrideWalk() const {
        double heel_strike = 0.05;
        double heel_lift = 0.05;
        double swing_flat = 0.35;
        double swing_toe = 0.05;

        auto times = {heel_lift, swing_flat, swing_toe, heel_strike, heel_lift,
                      swing_flat, swing_toe, heel_strike,};

        auto phase_contacts = {PB_, PP_, IP_, bP_, bB_, bb_, Ib_, Pb_};

        return std::make_pair(times, phase_contacts);
    }

    /**
     * Regular relaxed walk (start, half swing duration first)
     */
    GaitGeneratorBipedFeet::GaitInfo GaitGeneratorBipedFeet::GetStrideWalkBegin() const {
        double heel_strike = 0.05;
        double heel_lift = 0.05;
        double swing_flat = 0.35;
        double swing_toe = 0.05;

        auto times = {heel_lift, 0.5 * swing_flat, 0.5 * swing_toe, heel_strike,
                      heel_lift, swing_flat, swing_toe, heel_strike,};

        auto phase_contacts = {PB_, PP_, IP_, bP_, bB_, bb_, Ib_, Pb_};

        return std::make_pair(times, phase_contacts);
    }

    /**
     * Regular relaxed walk (end, half swing duration last)
     */
    GaitGeneratorBipedFeet::GaitInfo GaitGeneratorBipedFeet::GetStrideWalkEnd() const {
        double heel_strike = 0.05;
        double heel_lift = 0.10;
        double swing_flat = 0.35;
        double swing_toe = 0.05;

        auto times = {heel_lift, swing_flat, swing_toe, heel_strike, heel_lift, 0.5
                                                                                * swing_flat};

        auto phase_contacts = {PB_, PP_, IP_, bP_, bB_, bb_};
        // End flat with one foot, stance should follow this

        return std::make_pair(times, phase_contacts);
    }

    /**
     * Tip-toe-ing
     */
    GaitGeneratorBipedFeet::GaitInfo GaitGeneratorBipedFeet::GetStrideWalkTiptoe() const {
        double step = 0.3;
        double stance = 0.1;
        auto times = {step, stance, step, stance,};
        auto phase_contacts = {Ib_, IB_, // swing left foot
                               IP_, IB_, // swing right foot
        };

        return std::make_pair(times, phase_contacts);
    }

    /**
     * Sneaky walk with a lot of ground contact
     */
    GaitGeneratorBipedFeet::GaitInfo GaitGeneratorBipedFeet::GetStrideWalkSneak() const {
        double heel_strike = 0.1;
        double toe_lift = 0.1;
        double swing_flat = 0.25;

        auto times = {toe_lift, swing_flat, heel_strike, toe_lift, swing_flat,
                      heel_strike,};

        auto phase_contacts = {PB_, PP_, BB_, bB_, bb_, BB_};

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorBipedFeet::GaitInfo GaitGeneratorBipedFeet::GetStrideRun() const {
        double flight = 0.4;
        double pushoff = 0.15;
        double landing = 0.15;
        auto times = {flight + pushoff + landing};
        auto phase_contacts = {BB_};

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorBipedFeet::GaitInfo GaitGeneratorBipedFeet::GetStrideHop() const {
        double push = 0.15;
        double flight = 0.5;
        double land = 0.15;

        auto times = {push + flight + land};
        auto phase_contacts = {BB_};

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorBipedFeet::GaitInfo GaitGeneratorBipedFeet::GetStrideGallopHop() const {
        double push = 0.2;
        double flight = 0.3;
        double land = 0.2;

        auto times = {push + flight + land};
        auto phase_contacts = {BB_};

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorBipedFeet::GaitInfo GaitGeneratorBipedFeet::GetStrideLeftHop() const {
        double push = 0.15;
        double flight = 0.4;
        double land = 0.15;

        auto times = {push + flight + land};
        auto phase_contacts = {BB_};

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorBipedFeet::GaitInfo GaitGeneratorBipedFeet::GetStrideRightHop() const {
        double push = 0.2;
        double flight = 0.2;
        double land = 0.2;

        auto times = {push + flight + land};
        auto phase_contacts = {BB_};

        return std::make_pair(times, phase_contacts);
    }

    GaitGeneratorBipedFeet::GaitInfo GaitGeneratorBipedFeet::GetStrideBalance() const {
        const double stand = 0.6;

        auto times = {stand};
        auto phase_contacts = {PP_};

        return std::make_pair(times, phase_contacts);
    }

} /* namespace gambol */

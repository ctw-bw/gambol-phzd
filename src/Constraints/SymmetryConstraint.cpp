#include <gambol/Constraints/SymmetryConstraint.h>

#include <numeric>

namespace gambol {

    // Constructor
    SymmetryConstraint::SymmetryConstraint(const NodesVariables::Ptr& nodes,
                                           const std::vector<int>& dims) :
            ConstraintSet(kSpecifyLater, "symmetry-" + nodes->GetName()) {
        nodes_ = nodes;

        if (dims.empty()) {
            S_f_ = MatrixXd::Identity(nodes_->GetDim(), nodes_->GetDim());
        } else {
            S_f_ = MatrixXd::Zero(dims.size(), nodes_->GetDim());

            int i = 0;
            for (int dim : dims) {

                S_f_(i, dim) = 1.0; // Add ones according to the provided indices
                i++;
            }
        }

        S_0_ = S_f_; // Copy to second matrix

        Initialize();
    }

    // Constructor
    SymmetryConstraint::SymmetryConstraint(const NodesVariables::Ptr& nodes,
                                           const MatrixXd& S_f,
                                           const MatrixXd& S_0) :
            ConstraintSet(kSpecifyLater, "symmetry-" + nodes->GetName()) {
        nodes_ = nodes;

        S_f_ = S_f;

        if (S_0.size() == 0) {
            S_0_ = MatrixXd::Identity(S_f_.rows(), nodes->GetDim());
        } else {
            S_0_ = S_0;
        }

        Initialize();
    }

    // Initializer
    void SymmetryConstraint::Initialize() {

        assert(S_f_.rows() == S_0_.rows());
        assert(S_f_.cols() == nodes_->GetDim());
        assert(S_0_.cols() == nodes_->GetDim());

        n_dims_ = (int)S_f_.rows();

        jac_f_ = S_f_.sparseView(); // Matrices are constant, so we can safely get the sparse version
        jac_0_ = S_0_.sparseView();

        SetRows(n_dims_); // Just the dimensions
    }

    // Get constraint values
    SymmetryConstraint::VectorXd SymmetryConstraint::GetValues() const {

        VectorXd g = VectorXd(GetRows());

        const int k_end = nodes_->GetNumNodes() - 1;

        g = S_f_ * nodes_->GetNode(k_end) - S_0_ * nodes_->GetNode(0);

        return g;
    }

    // Get bounds value
    SymmetryConstraint::VecBound SymmetryConstraint::GetBounds() const {
        VecBound bounds(GetRows());

        for (auto& bound : bounds) {
            bound = ifopt::BoundZero;
        }

        return bounds;
    }

    // Get jacobian section values
    void SymmetryConstraint::FillJacobianBlock(std::string var_set, Jacobian& jac) const {
        if (var_set == nodes_->GetName()) {
            const int k_end = nodes_->GetNumNodes() - 1;

            /*int i = 0;
            for (auto dim : n_dims_) {
                int idx_start = nodes_->GetOptIndex(0, dim);
                int idx_end = nodes_->GetOptIndex(k_end, dim);

                jac.coeffRef(i, idx_end) = 1.0;
                jac.coeffRef(i, idx_start) = -1.0;
                i++;
            }*/

            // Copy elements from sparse matrices
            for (int m = 0; m < jac_0_.outerSize(); m++) {
                for (Jacobian::InnerIterator it(jac_0_, m); it; ++it) {

                    int idx = nodes_->GetOptIndex(0, (int)it.col());

                    jac.coeffRef(m, idx) = -it.value();
                }
            }
            for (int m = 0; m < jac_f_.outerSize(); m++) {
                for (Jacobian::InnerIterator it(jac_f_, m); it; ++it) {

                    int idx = nodes_->GetOptIndex(k_end, (int)it.col());

                    jac.coeffRef(m, idx) = it.value();
                }
            }
        }
    }

} /* namespace gambol */

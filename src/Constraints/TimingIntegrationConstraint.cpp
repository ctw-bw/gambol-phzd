#include <gambol/Constraints/TimingIntegrationConstraint.h>
#include <gambol/Variables/VariablesNames.h>

namespace gambol {

    // Constructor
    TimingIntegrationConstraint::TimingIntegrationConstraint(const NodesHolder& nodes_holder) :
            NodesConstraint(nodes_holder, "timing-integration") {

        // Link all adjacent couples, so skip the last node (both velocity and acceleration)
        SetRows((GetNumberOfNodes() - 1) * 2);

        dt_k_ = 0.0;
    }

    // Get row number
    int TimingIntegrationConstraint::GetRow(int k, int type) const {
        // Only constraints between nodes
        assert(k < GetNumberOfNodes() - 1 && "Index exceeds number of constraints");

        return k * 2 + type;
    }

    // Update models
    bool TimingIntegrationConstraint::Update(int k) const {
        if (k >= GetNumberOfNodes() - 1) {
            return false;
        }

        dt_k_ = nodes_holder_.node_times_->GetDeltaT(k);

        return true;
    }

    // Get constraint value
    void TimingIntegrationConstraint::UpdateConstraintAtNode(int k, VectorXd& g) const {
        if (!Update(k)) {
            return; // Nothing to do
        }

        const auto& time = nodes_holder_.time_;
        const auto& time_dt = nodes_holder_.time_dt_;
        const auto& time_ddt = nodes_holder_.time_ddt_;

        g[GetRow(k, 0)] = time->GetNode(k)[0] - time->GetNode(k + 1)[0]
                + 0.5 * dt_k_ * (time_dt->GetNode(k)[0] + time_dt->GetNode(k + 1)[0]);
        g[GetRow(k, 1)] = time_dt->GetNode(k)[0] - time_dt->GetNode(k + 1)[0]
                          + 0.5 * dt_k_ * (time_ddt->GetNode(k)[0] + time_ddt->GetNode(k + 1)[0]);
    }

    // Get bounds value
    void TimingIntegrationConstraint::UpdateBoundsAtNode(int k, VecBound& bounds) const {
        if (k < GetNumberOfNodes() - 1) {
            bounds[GetRow(k, 0)] = ifopt::BoundZero;
            bounds[GetRow(k, 1)] = ifopt::BoundZero;
        }
    }

    // Get jacobian value
    void TimingIntegrationConstraint::UpdateJacobianAtNode(int k, std::string var_set,
                                                     Jacobian& jac) const {
        if (!Update(k)) {
            return; // Nothing to do
        }

        const auto& time = nodes_holder_.time_;
        const auto& time_dt = nodes_holder_.time_dt_;
        const auto& time_ddt = nodes_holder_.time_ddt_;

        int n = (int)jac.cols(); // Number of columns of jacobian (= size of variable set)
        Jacobian jac_vel_k(1, n);
        Jacobian jac_acc_k(1, n);

        if (var_set == time->GetName()) {

            Jacobian node_jac_k = time->GetNodeJacobian(k);
            Jacobian node_jac_kp1 = time->GetNodeJacobian(k + 1);

            // time[k] - time[k+1]
            jac_vel_k = node_jac_k - node_jac_kp1;

        } else if (var_set == time_dt->GetName()) {

            // 0.5 * dt * (time_dt[k] + time_dt[k+1])
            jac_vel_k = (time_dt->GetNodeJacobian(k) + time_dt->GetNodeJacobian(k + 1)) * 0.5 * dt_k_;

            // time_dt[k] - time_dt[k + 1] ...
            jac_acc_k = time_dt->GetNodeJacobian(k) - time_dt->GetNodeJacobian(k + 1);

        } else if (var_set == time_ddt->GetName()) {
            // ... 0.5 * dt * (time_ddt[k] + time_ddt[k+1])
            jac_acc_k = (time_ddt->GetNodeJacobian(k) + time_ddt->GetNodeJacobian(k + 1)) *
                    0.5 * dt_k_;
        } else {
            return; // Not relevant variable
        }

        jac.row(GetRow(k, 0)) = jac_vel_k;
        jac.row(GetRow(k, 1)) = jac_acc_k;
    }

} /* namespace gambol */

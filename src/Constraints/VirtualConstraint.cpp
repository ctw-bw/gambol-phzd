#include <gambol/Constraints/VirtualConstraint.h>

#include <iostream>

namespace gambol {

    // Constructor
    VirtualConstraint::VirtualConstraint(const NodesHolder& s,
                                         double v_d,
                                         const MatrixXd& J,
                                         MatrixXd H) :
            NodesConstraint(s, "virtual") {

        v_d_ = v_d;

        epsilon_ = 10.0;

        size_q_ = nodes_holder_.joint_pos_->GetDim();
        size_dq_ = nodes_holder_.joint_vel_->GetDim();
        size_u_ = nodes_holder_.torques_->GetDim();

        assert(J.rows() == 1 && "Simplified jacobian must be a row vector");
        assert(J.cols() == size_q_ && "Simplified jacobian must have columns equal to #q");

        /*
         * Joint position and joint velocity often have different dimensions.
         * This is solved by considering q_dot = B * v
         * Here B is _not_ a real rates jacobian, any base orientation is skipped.
         */

        J_ = J;

        if (H.size() == 0) { // Default argument passed

            // Assume that that the last `size_u_` coordinates are actuated
            H = MatrixXd(size_u_, size_q_);
            H << MatrixXd::Zero(size_u_, size_q_ - size_u_),
                    MatrixXd::Identity(size_u_, size_u_);
        }

        assert(H.cols() == size_q_ && "Selection matrix must be multiplied with joint position");

        H_ = H;

        size_y2_ = (int)H_.rows();

        if (size_q_ == size_dq_) {
            B_inv_ = MatrixXd::Identity(size_q_, size_q_);
        } else {
            B_inv_ = MatrixXd::Zero(size_dq_, size_q_);
            // Set the non-base part to identity
            B_inv_.block(6, 7, size_dq_ - 6, size_dq_ - 6).setIdentity();
        }

        SetRows(GetNumberOfNodes() * (1 + size_y2_) + 2 * size_y2_); // Two constraints per node plus two static constraints
    }

    // Get constraint index
    int VirtualConstraint::GetRow(int k, int type, int dim) const {
        if (type == 0) {
            return k * (1 + size_y2_);
        } else if (type == 1) {
            return k * (1 + size_y2_) + 1 + dim;
        } else if (type == 2) {
            return GetRows() - size_y2_ * 2 + dim;
        } else if (type == 3) {
            return GetRows() - size_y2_ * 1 + dim;
        }

        assert(false && "`type` must be less than four");
    }

    // Get constraint values
    Eigen::VectorXd VirtualConstraint::GetValues() const {

        VectorXd g = VectorXd::Zero(GetRows());

        for (int k = 0; k < GetNumberOfNodes(); k++) {
            UpdateConstraintAtNode(k, g);
        }

        UpdateExtraConstraint(g);

        return g;
    }

    // Get bounds of the constraint
    VirtualConstraint::VecBound VirtualConstraint::GetBounds() const {

        // All different constraints are zero
        VecBound bounds(GetRows());

        for (int k = 0; k < GetNumberOfNodes(); k++) {
            UpdateBoundsAtNode(k, bounds);
        }

        UpdateExtraBounds(bounds);

        return bounds;
    }

    void VirtualConstraint::UpdateBoundsAtNode(int k, VecBound& bounds) const {

        //bounds[GetRow(k, 0)] = Bounds(-1.0e19, 1.0e19);
        bounds[GetRow(k, 0)] = ifopt::BoundZero;

        for (int i = 0; i < size_y2_; i++) {
            bounds[GetRow(k, 1, i)] = ifopt::BoundZero;
        }

    }

    void VirtualConstraint::UpdateExtraBounds(VecBound& bounds) const {

        for (int i = 0; i < size_y2_; i++) {
            bounds[GetRow(0, 2, i)] = ifopt::BoundZero;
            bounds[GetRow(0, 3, i)] = ifopt::BoundZero;
        }

    }

    // Get section of the jacobian
    void VirtualConstraint::FillJacobianBlock(std::string var_set,
                                            Jacobian& jac) const {
        for (int k = 0; k < GetNumberOfNodes(); k++) {
            UpdateJacobianAtNode(k, var_set, jac);
        }

        UpdateExtraJacobian(var_set, jac);
    }

    // Get node constraint values
    void VirtualConstraint::UpdateConstraintAtNode(int k, VectorXd& g) const {

        const VectorXd q_k = B_inv_ * nodes_holder_.joint_pos_->GetNode(k);
        const VectorXd& dq_k = nodes_holder_.joint_vel_->GetNode(k);
        const VectorXd& ddq_k = nodes_holder_.joint_acc_->GetNode(k);
        const double s_k = nodes_holder_.time_->GetNode(k)[0];
        const double ds_k = nodes_holder_.time_dt_->GetNode(k)[0];
        const double dds_k = nodes_holder_.time_ddt_->GetNode(k)[0];

        // First constraint
        MatrixXd acc_plus_vel = J_ * ddq_k + J_ * dq_k * epsilon_; // Result is scalar, but Eigen doesn't support that
        g(GetRow(k, 0)) = acc_plus_vel(0,0) - v_d_;

        // Second constraint
        const auto& beziers = nodes_holder_.beziers_;

        VectorXd b_k = beziers->GetCurveVector(s_k);
        VectorXd db_k = beziers->GetCurveVectorDot(s_k);
        VectorXd ddb_k = beziers->GetCurveVectorDotDot(s_k);

        VectorXd b_dot_k = db_k * ds_k;
        VectorXd b_ddot_k = ddb_k * pow(ds_k, 2) + db_k * dds_k;

        g.segment(GetRow(k, 1), size_y2_) = H_ * ddq_k - b_ddot_k
                                            + 2 * epsilon_ * (H_ * dq_k - b_dot_k)
                                            + pow(epsilon_, 2) * (H_ * q_k - b_k);

        // See UpdateExtraConstraint() for third and fourth constraints
    }

    void VirtualConstraint::UpdateJacobianAtNode(int k, std::string var_set, Jacobian& jac) const {

        const auto& joint_pos = nodes_holder_.joint_pos_;
        if (var_set == joint_pos->GetName()) {

            // Second constraint, ... epsilon^2 * H * q ...
            MatrixXd H_dense = H_ * B_inv_;
            jac.middleRows(GetRow(k, 1), size_y2_) = H_dense.sparseView()
                    * joint_pos->GetNodeJacobian(k) * pow(epsilon_, 2);
        }

        const auto& joint_vel = nodes_holder_.joint_vel_;
        if (var_set == joint_vel->GetName()) {

            // First constraint, g = J * ddq_k + epsilon * J * dq_k - epsilon * v_d
            jac.row(GetRow(k, 0)) = J_.sparseView() * joint_vel->GetNodeJacobian(k) * epsilon_;

            // Second constraint, ... 2 * epsilon * H * dq ...
            jac.middleRows(GetRow(k, 1), size_y2_) = H_.sparseView()
                                    * joint_vel->GetNodeJacobian(k) * 2.0 * epsilon_;
        }

        const auto& joint_acc = nodes_holder_.joint_acc_;
        if (var_set == joint_acc->GetName()) {

            // First constraint, g = J * ddq_k + J * dq_k - v_d
            jac.row(GetRow(k, 0)) = J_.sparseView() * joint_acc->GetNodeJacobian(k);

            // Second constraint, ... H * ddq ...
            jac.middleRows(GetRow(k, 1), size_y2_) = H_.sparseView()
                                                     * joint_acc->GetNodeJacobian(k);
        }

        const auto& time = nodes_holder_.time_;
        const auto& time_dt = nodes_holder_.time_dt_;
        const auto& time_ddt = nodes_holder_.time_ddt_;
        const auto& beziers = nodes_holder_.beziers_;

        const double s_k = time->GetNode(k)[0];
        const double ds_k = time_dt->GetNode(k)[0];
        const double dds_k = time_ddt->GetNode(k)[0];

        VectorXd b_k = beziers->GetCurveVector(s_k);
        VectorXd db_k = beziers->GetCurveVectorDot(s_k);
        VectorXd ddb_k = beziers->GetCurveVectorDotDot(s_k);
        VectorXd dddb_k = beziers->GetCurveVectorDotDotDot(s_k);

        if (var_set == time->GetName()) {

            // Second constraint

            Jacobian jac_ds(size_y2_, 1);

            for (int i = 0; i < size_y2_; i++) {
                jac_ds.insert(i, 0) = -dddb_k[i] * pow(ds_k, 2) - ddb_k[i] * dds_k
                        - 2.0 * epsilon_ * ddb_k[i] * ds_k
                        - pow(epsilon_, 2) * db_k[i];
            }

            jac.middleRows(GetRow(k, 1), size_y2_) = jac_ds * time->GetNodeJacobian(k);
        }

        if (var_set == time_dt->GetName()) {

            // Second constraint

            Jacobian jac_ds(size_y2_, 1);

            for (int i = 0; i < size_y2_; i++) {
                jac_ds.insert(i, 0) = -2.0 * ddb_k[i] * ds_k - 2.0 * epsilon_ * db_k[i];
            }

            jac.middleRows(GetRow(k, 1), size_y2_) = jac_ds * time_dt->GetNodeJacobian(k);
        }

        if (var_set == time_ddt->GetName()) {

            // Second constraint

            Jacobian jac_ds(size_y2_, 1);

            for (int i = 0; i < size_y2_; i++) {
                jac_ds.insert(i, 0) = -db_k[i];
            }

            jac.middleRows(GetRow(k, 1), size_y2_) = jac_ds * time_ddt->GetNodeJacobian(k);
        }


        if (var_set == beziers->GetName()) {

            // Second constraint

            Jacobian jac_dalpha = -beziers->GetDotDotCoefficientJacobian(s_k) * pow(ds_k, 2)
                     - beziers->GetDotCoefficientJacobian(s_k) * (dds_k + ds_k * 2.0 * epsilon_)
                     - beziers->GetCoefficientJacobian(s_k) * pow(epsilon_, 2);

            jac.middleRows(GetRow(k, 1), size_y2_) = jac_dalpha;
        }

    }

    // Get the third and second constraint
    void VirtualConstraint::UpdateExtraConstraint(VectorXd& g) const {

        const VectorXd q_k = B_inv_ * nodes_holder_.joint_pos_->GetNode(0);
        const VectorXd& dq_k = nodes_holder_.joint_vel_->GetNode(0);
        const double ds_k = nodes_holder_.time_dt_->GetNode(0)[0];

        const auto& beziers = nodes_holder_.beziers_;
        VectorXd b_k = beziers->GetCurveVector(0.0); // First node, so s = 0
        VectorXd db_k = beziers->GetCurveVectorDot(0.0);
        VectorXd b_dot_k = db_k * ds_k;

        // Third constraint
        g.segment(GetRow(0, 2), size_y2_) = H_ * q_k - b_k;

        // Fourth constraint
        g.segment(GetRow(0, 3), size_y2_) = H_ * dq_k - b_dot_k;
    }

    void VirtualConstraint::UpdateExtraJacobian(std::string var_set, Jacobian& jac) const {

        const auto& beziers = nodes_holder_.beziers_;

        const auto& joint_pos = nodes_holder_.joint_pos_;
        if (var_set == joint_pos->GetName()) {

            MatrixXd jac_2_dense = H_ * B_inv_;
            // Only H_

            jac.middleRows(GetRow(0, 2), size_y2_) =
                    jac_2_dense.sparseView() * joint_pos->GetNodeJacobian(0);
            // We can safely use sparseView here because the matrices are constant during optimization

            // Fourth constraint is not dependent on q
        }

        const auto& joint_vel = nodes_holder_.joint_vel_;
        if (var_set == joint_vel->GetName()) {
            // Third constraint is not dependent on dq

            MatrixXd jac_3_dense = H_;
            // Only H_

            jac.middleRows(GetRow(0, 3), size_y2_) =
                    jac_3_dense.sparseView() * joint_vel->GetNodeJacobian(0);
        }

        if (var_set == beziers->GetName()) {

            // Third constraint
            jac.middleRows(GetRow(0, 2), size_y2_) = -beziers->GetCoefficientJacobian(0.0);

            // Fourth constraint
            const double ds_k = nodes_holder_.time_dt_->GetNode(0)[0];
            jac.middleRows(GetRow(0, 3), size_y2_) = -beziers->GetDotCoefficientJacobian(0.0) * ds_k;
        }

        const auto time_dt = nodes_holder_.time_dt_;
        if (var_set == time_dt->GetName()) {

            // Fourth constraint
            Jacobian jac_ds(size_y2_, 1);

            const VectorXd db_k = nodes_holder_.beziers_->GetCurveVectorDot(0.0);

            for (int i = 0; i < size_y2_; i++) {
                jac_ds.insert(i, 0) = -db_k[i];
            }

            jac.middleRows(GetRow(0, 3), size_y2_) = jac_ds * time_dt->GetNodeJacobian(0);;
        }
    }
}

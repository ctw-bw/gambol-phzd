#include <gambol/Constraints/ForceConstraint.h>
#include <gambol/Variables/VariablesNames.h>
#include <gambol/Options/TerrainOption.h>

namespace gambol {

    // Constructor
    ForceConstraint::ForceConstraint(const HeightMap::Ptr& terrain,
                                     const RobotModel::Ptr& model, const NodesHolder& nodes_holder,
                                     double max_normal, uint ee_id) :
            NodesConstraint(nodes_holder, "forcelimits-" + id::EEForceNodes(ee_id)) {
        max_normal_force_ = max_normal;
        ee_id_ = ee_id;

#if TERRAIN_FLAT
        terrain_ = nullptr; // Make sure we really don't use it
        model_ = nullptr;

        (void)model; // Avoid 'unused' warning
#else
        terrain_ = terrain;
        model_ = model;
#endif

        if (terrain) {
            mu_ = terrain->GetFrictionCoeff(); // Get coeff from terrain
        } else {
            mu_ = 0.5; // Hard-coded default, since terrain is optional
        }

        SetRows(5 * GetNumberOfNodes());
        // Three constraints per node (z-value and two for two slopes)
    }

    // Get row number
    int ForceConstraint::GetRow(int k, int type) const {
        // Only constraints between nodes
        assert(k < GetNumberOfNodes() && "Index exceeds number of constraints");

        return (k * 5) + type;
    }

    // Update
    void ForceConstraint::Update(int k) const {
        model_->SetCurrent(nodes_holder_, k);
    }

    // Get constraint value
    void ForceConstraint::UpdateConstraintAtNode(int k, VectorXd& g) const {

        Vector3d force = nodes_holder_.ee_forces_[ee_id_]->GetNode(k);

#if TERRAIN_FLAT
        // For a flat terrain the constraints are pretty much static:
        g[GetRow(k, 0)] = force[2]; // z-force
        g[GetRow(k, 1)] = force[0] - mu_ * force[2]; // < 0
        g[GetRow(k, 2)] = force[0] + mu_ * force[2]; // > 0
        g[GetRow(k, 3)] = force[1] - mu_ * force[2]; // < 0
        g[GetRow(k, 4)] = force[1] + mu_ * force[2]; // > 0

#else
        // For a non-flat terrain we need the terrain normals and tangents
        Update(k);

        Vector3d ee_pos = model_->GetEEPos(ee_id_);
        Vector3d normal = terrain_->GetNormalizedBasis(HeightMap::Normal, ee_pos);

        // Unilateral force
        g[GetRow(k, 0)] = force.dot(normal); // Normal force

        // Friction pyramid
        Vector3d t1 = terrain_->GetNormalizedBasis(HeightMap::Tangent1, ee_pos);
        g[GetRow(k, 1)] = force.dot(t1 - mu_ * normal); // < 0
        g[GetRow(k, 2)] = force.dot(t1 + mu_ * normal); // > 0

        Vector3d t2 = terrain_->GetNormalizedBasis(HeightMap::Tangent2, ee_pos);
        g[GetRow(k, 3)] = force.dot(t2 - mu_ * normal); // < 0
        g[GetRow(k, 4)] = force.dot(t2 + mu_ * normal); // > 0
#endif
    }

    // Get bounds value
    void ForceConstraint::UpdateBoundsAtNode(int k, VecBound& bounds) const {
        // z-position
        double t_k = nodes_holder_.node_times_->at(k);
        bool contact = nodes_holder_.phase_durations_[ee_id_]->IsContactPhase(t_k);

        if (contact) {
            bounds.at(GetRow(k, 0)) = ifopt::Bounds(0.0, max_normal_force_); // normal
            bounds.at(GetRow(k, 1)) = ifopt::BoundSmallerZero; // f_t1 <   mu*n
            bounds.at(GetRow(k, 2)) = ifopt::BoundGreaterZero; // f_t1 >  -mu*n
            bounds.at(GetRow(k, 3)) = ifopt::BoundSmallerZero; // f_t2 <   mu*n
            bounds.at(GetRow(k, 4)) = ifopt::BoundGreaterZero; // f_t2 >  -mu*n
        } else {
            // Swing, no reaction forces
            bounds.at(GetRow(k, 0)) = ifopt::BoundZero;
            bounds.at(GetRow(k, 1)) = ifopt::BoundZero;
            bounds.at(GetRow(k, 2)) = ifopt::BoundZero;
            bounds.at(GetRow(k, 3)) = ifopt::BoundZero;
            bounds.at(GetRow(k, 4)) = ifopt::BoundZero;
        }
    }

    // Get jacobian
    void ForceConstraint::UpdateJacobianAtNode(int k, std::string var_set,
                                               Jacobian& jac) const {

        auto forces = nodes_holder_.ee_forces_[ee_id_];

#if TERRAIN_FLAT
        // For flat terrain we only need to consider the reaction forces themselves

        if (var_set == forces->GetName()) {
            int idx_x = forces->GetOptIndex(k, 0);
            int idx_y = forces->GetOptIndex(k, 1);
            int idx_z = forces->GetOptIndex(k, 2);

            jac.coeffRef(GetRow(k, 0), idx_z) = 1.0; // force_z

            jac.coeffRef(GetRow(k, 1), idx_x) = 1.0; // force_x
            jac.coeffRef(GetRow(k, 1), idx_z) = -mu_; // -force_z

            jac.coeffRef(GetRow(k, 2), idx_x) = 1.0; // force_x
            jac.coeffRef(GetRow(k, 2), idx_z) = mu_; // +force_z

            jac.coeffRef(GetRow(k, 3), idx_y) = 1.0; // force_y
            jac.coeffRef(GetRow(k, 3), idx_z) = -mu_; // -force_z

            jac.coeffRef(GetRow(k, 4), idx_y) = 1.0; // force_y
            jac.coeffRef(GetRow(k, 4), idx_z) = mu_; // +force_z
        }

#else
        // For non-flat terrain the end-effector position and the terrain shape are also relevant

        auto joint_pos = nodes_holder_.joint_pos_;

        if (var_set != forces->GetName() && var_set != joint_pos->GetName()) {
            return; // Nothing to do
        }

        Update(k);

        if (var_set == forces->GetName()) {
            Vector3d ee_pos = model_->GetEEPos(ee_id_);
            Vector3d normal = terrain_->GetNormalizedBasis(HeightMap::Normal,
                                                           ee_pos);
            Vector3d t1 = terrain_->GetNormalizedBasis(HeightMap::Tangent1, ee_pos);
            Vector3d t2 = terrain_->GetNormalizedBasis(HeightMap::Tangent2, ee_pos);

            Jacobian jac_k(5, 3);

            // Loop over dimensions
            for (int dim : {X, Y, Z}) {
                jac_k.coeffRef(0, dim) = normal[dim]; // Normal
                jac_k.coeffRef(1, dim) = t1[dim] - mu_ * normal[dim]; // f_t1 <  mu*n
                jac_k.coeffRef(2, dim) = t1[dim] + mu_ * normal[dim]; // f_t1 > -mu*n
                jac_k.coeffRef(3, dim) = t2[dim] - mu_ * normal[dim]; // f_t2 <  mu*n
                jac_k.coeffRef(4, dim) = t2[dim] + mu_ * normal[dim]; // f_t2 > -mu*n
            }

            jac.middleRows(GetRow(k), 5) = jac_k * forces->GetNodeJacobian(k);
        }

        if (var_set == joint_pos->GetName()) {
            Vector3d ee_pos = model_->GetEEPos(ee_id_);
            Vector3d force = forces->GetNode(k);

            Jacobian dn = terrain_->GetDerivativeOfNormalizedBasis(
                    HeightMap::Normal, ee_pos);
            Jacobian dt1 = terrain_->GetDerivativeOfNormalizedBasis(
                    HeightMap::Tangent1, ee_pos);
            Jacobian dt2 = terrain_->GetDerivativeOfNormalizedBasis(
                    HeightMap::Tangent2, ee_pos);

            Jacobian F_trans(1, 3);
            F_trans.insert(0, 0) = force[0];
            F_trans.insert(0, 1) = force[1];
            F_trans.insert(0, 2) = force[2];

            Jacobian jac_k(5, 3);
            jac_k.row(0) = F_trans * dn;
            jac_k.row(1) = F_trans * (dt1 - mu_ * dn);
            jac_k.row(2) = F_trans * (dt1 + mu_ * dn);
            jac_k.row(3) = F_trans * (dt2 - mu_ * dn);
            jac_k.row(4) = F_trans * (dt2 + mu_ * dn);

            Jacobian ee_diff_q = model_->GetEEPosJacobian(ee_id_);

            Jacobian new_jac = jac_k * ee_diff_q * joint_pos->GetNodeJacobian(k);

            jac.middleRows(GetRow(k), 5) = new_jac;
        }
#endif
    }

} /* namespace gambol */

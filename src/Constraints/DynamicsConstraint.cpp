#include <memory>
#include <gambol/Constraints/DynamicsConstraint.h>

namespace gambol {

    // Constructor
    DynamicsConstraint::DynamicsConstraint(const RobotModel::Ptr& model,
                                           const NodesHolder& s) :
            NodesConstraint(s, "dynamics") {

        model_ = model;

        size_q_ = nodes_holder_.joint_pos_->GetDim();
        size_dq_ = nodes_holder_.joint_vel_->GetDim();

        // Simply link all nodes
        SetRows(GetNumberOfNodes() * size_dq_);
    }

    // GetRow
    int DynamicsConstraint::GetRow(int k, int dim) const {
        // Only constraints between nodes
        assert(k < GetNumberOfNodes() && "Index exceeds number of constraints");

        return (k * size_dq_) + dim;
    }

    // Update
    void DynamicsConstraint::Update(int k) const {

        model_->SetCurrent(nodes_holder_, k);
    }

    // Get constraint value
    void DynamicsConstraint::UpdateConstraintAtNode(int k, VectorXd& g) const {
        Update(k);

        VectorXd dqq_k = nodes_holder_.joint_acc_->GetNode(k); // Current dynamics value

        VectorXd ddq = model_->GetDynamics(); // Calculated dynamics

        VectorXd error = ddq - dqq_k;

        g.segment(GetRow(k), error.size()) = error;
    }

    // Get bounds value
    void DynamicsConstraint::UpdateBoundsAtNode(int k, VecBound& bounds) const {
        for (int i = 0; i < size_dq_; i++) {
            bounds[GetRow(k, i)] = ifopt::BoundZero;
        }
    }

    // Get jacobian value
    void DynamicsConstraint::UpdateJacobianAtNode(int k, std::string var_set,
                                                  Jacobian& jac) const {
        Update(k);

        int n = (int)jac.cols(); // Number of columns of jacobian (= size of variable set)
        Jacobian jak_acc_k(size_dq_, n);

        auto joint_pos = nodes_holder_.joint_pos_;
        if (var_set == joint_pos->GetName()) {
            Jacobian ddq_diff_qpos = model_->GetDynamicsJacobianWrtPos();

            // ... dynamics[k] ...
            jak_acc_k = ddq_diff_qpos * joint_pos->GetNodeJacobian(k);
        }

        auto joint_vel = nodes_holder_.joint_vel_;
        if (var_set == joint_vel->GetName()) {
            Jacobian ddq_diff_qvel = model_->GetDynamicsJacobianWrtVel();

            // ... dynamics[k] ...
            jak_acc_k = ddq_diff_qvel * joint_vel->GetNodeJacobian(k);
        }

        auto joint_acc = nodes_holder_.joint_acc_;
        if (var_set == joint_acc->GetName()) {

            Jacobian identity(size_dq_, size_dq_);
            identity.setIdentity();

            // ... - ddq[k] ...
            jak_acc_k = -identity * joint_acc->GetNodeJacobian(k);
        }

        auto torques = nodes_holder_.torques_;
        if (var_set == torques->GetName()) {
            Jacobian ddq_diff_torque = model_->GetDynamicsJacobianWrtTorque();

            // ... dynamics[k] ...
            jak_acc_k = ddq_diff_torque * torques->GetNodeJacobian(k);
        }

        for (int ee = 0; ee < model_->GetEECount(); ee++) {
            auto forces = nodes_holder_.ee_forces_[ee];

            if (var_set == forces->GetName()) {
                Jacobian ddq_diff_jac_forces = model_->GetDynamicsJacobianWrtForces(ee);

                // ... dynamics[k] ...
                jak_acc_k = ddq_diff_jac_forces * forces->GetNodeJacobian(k);
            }
        }

        jac.middleRows(GetRow(k), size_dq_) = jak_acc_k;
    }

} /* namespace gambol */

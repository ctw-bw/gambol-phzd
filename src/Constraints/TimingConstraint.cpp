#include <memory>
#include <gambol/Constraints/TimingConstraint.h>
#include <gambol/Robots/RobotModel.h>

namespace gambol {

    // Constructor
    TimingConstraint::TimingConstraint(const NodesHolder& nodes_holder, const MatrixXd& J) :
            NodesConstraint(nodes_holder, "timing") {

        size_q_ = nodes_holder_.joint_pos_->GetDim();
        size_dq_ = nodes_holder_.joint_vel_->GetDim();

        assert(J.rows() == 1 && "Simplified jacobian must be a row vector");
        assert(J.cols() == size_q_ && "Simplified jacobian must have columns equal to #q");

        /*
         * Joint position and joint velocity often have different dimensions.
         * This is solved by considering q_dot = B * v
         * Here B is _not_ a real rates jacobian: any base orientation is skipped.
         */

        J_ = J;

        if (size_q_ == size_dq_) {
            B_inv_ = MatrixXd::Identity(size_q_, size_q_);
        } else {
            B_inv_ = MatrixXd::Zero(size_dq_, size_q_);
            // Set the non-base part to identity
            B_inv_.block(6, 7, size_dq_ - 6, size_dq_ - 6).setIdentity();
        }

        S_mat_k_ = MatrixXd::Zero(1, size_q_);

        // Link time, time_dt and time_ddt for all nodes
        SetRows(GetNumberOfNodes() * 3);
    }

    // GetRow
    int TimingConstraint::GetRow(int k, int type) const {

        return k * 3 + type;
    }

    // Update
    void TimingConstraint::Update(int k) const {

        const int Nm1 = GetNumberOfNodes() - 1;

        const auto& q_0 = nodes_holder_.joint_pos_->GetNode(0);
        const auto& q_k = nodes_holder_.joint_pos_->GetNode(k);
        const auto& q_f = nodes_holder_.joint_pos_->GetNode(Nm1);

        MatrixXd num_mat = J_ * B_inv_ * (q_k - q_0);
        MatrixXd den_mat = J_ * B_inv_ * (q_f - q_0);

        double num = num_mat(0, 0);
        double den = den_mat(0, 0);  // Eigen cannot convert matrix to double

        // In the first iteration, q_f could be equal to q_0. The denominator will then be (close to) zero, which is
        // bad
        const double min = 0.0000001;
        if (den >= 0.0 && den < min) {
            den = min; // Limit to a reasonably small value
        } else if (den < 0.0 && den > -min) {
            den = -min;
        }

        // Timing must be 0 and 1 at start and end
        s_k_ = num / den;

        S_mat_k_ = 1.0 / den * J_;
    }

    // Get constraint value
    void TimingConstraint::UpdateConstraintAtNode(int k, VectorXd& g) const {

        Update(k);

        const auto& timing = nodes_holder_.time_;
        const auto& timing_dt = nodes_holder_.time_dt_;
        const auto& timing_ddt = nodes_holder_.time_ddt_;

        VectorXd ds_k = S_mat_k_ * nodes_holder_.joint_vel_->GetNode(k);
        VectorXd dds_k = S_mat_k_ * nodes_holder_.joint_acc_->GetNode(k);

        g[GetRow(k, 0)] = s_k_ - timing->GetNode(k)[0]; // Computed value minus variable value
        g[GetRow(k, 1)] = ds_k[0] - timing_dt->GetNode(k)[0];
        g[GetRow(k, 2)] = dds_k[0] - timing_ddt->GetNode(k)[0];
    }

    // Get bounds value
    void TimingConstraint::UpdateBoundsAtNode(int k, VecBound& bounds) const {
        for (int i = 0; i < 3; i++) {
            bounds[GetRow(k, i)] = ifopt::BoundZero;
        }
    }

    // Get jacobian value
    void TimingConstraint::UpdateJacobianAtNode(int k, std::string var_set,
                                                  Jacobian& jac) const {

        Update(k);

        const auto& timing = nodes_holder_.time_;
        if (var_set == timing->GetName()) {

            jac.row(GetRow(k, 0)) = -timing->GetNodeJacobian(k);
        }

        const auto& timing_dt = nodes_holder_.time_dt_;
        if (var_set == timing_dt->GetName()) {

            jac.row(GetRow(k, 1)) = -timing_dt->GetNodeJacobian(k);
        }

        const auto& timing_ddt = nodes_holder_.time_ddt_;
        if (var_set == timing_ddt->GetName()) {

            jac.row(GetRow(k, 2)) = -timing_ddt->GetNodeJacobian(k);
        }

        const auto& joint_pos = nodes_holder_.joint_pos_;
        const auto& joint_vel = nodes_holder_.joint_vel_;
        const auto& joint_acc = nodes_holder_.joint_acc_;
        if (var_set == joint_pos->GetName()) {

            const int Nm1 = GetNumberOfNodes() - 1;

            auto& q_0 = joint_pos->GetNode(0);
            auto& q_k = joint_pos->GetNode(k);

            Jacobian S = S_mat_k_.sparseView(); // S always has the shape of J_, so we can safely take the sparse
            // form

            // g_k w.r.t. q[k]
            Jacobian jac_qk = S * joint_pos->GetNodeJacobian(k);

            // g_k w.r.t. q[0]

            VectorXd grad_q0 = S_mat_k_.transpose() * S_mat_k_ * (q_k - q_0) - S_mat_k_.transpose();
            Jacobian grad_q0_sparse = RobotModel::DenseToSparse(grad_q0.transpose());
            Jacobian jac_q0 = grad_q0_sparse * joint_pos->GetNodeJacobian(0);

            // g_k w.r.t. q[N-1]
            VectorXd grad_qf = S_mat_k_.transpose() * S_mat_k_ * (q_0 - q_k);
            Jacobian grad_qf_sparse = RobotModel::DenseToSparse(grad_qf.transpose());
            Jacobian jac_qf = grad_qf_sparse * joint_pos->GetNodeJacobian(Nm1);

            jac.row(GetRow(k, 0)) = jac_qk + jac_q0 + jac_qf;

            // S * dq[k]

            MatrixXd grad_q0_vel = S_mat_k_.transpose() * S_mat_k_ * joint_vel->GetNode(k);
            Jacobian grad_q0_vel_sparse = RobotModel::DenseToSparse(grad_q0_vel.transpose());

            MatrixXd grad_qf_vel = -S_mat_k_.transpose() * S_mat_k_ * joint_vel->GetNode(k);
            Jacobian grad_qf_vel_sparse = RobotModel::DenseToSparse(grad_qf_vel.transpose());

            jac.row(GetRow(k, 1)) = grad_q0_vel_sparse * joint_pos->GetNodeJacobian(0)
                    + grad_qf_vel_sparse * joint_pos->GetNodeJacobian(Nm1);

            // S * ddq[k]

            MatrixXd grad_q0_acc = S_mat_k_.transpose() * S_mat_k_ * joint_acc->GetNode(k);
            Jacobian grad_q0_acc_sparse = RobotModel::DenseToSparse(grad_q0_acc.transpose());

            MatrixXd grad_qf_acc = -S_mat_k_.transpose() * S_mat_k_ * joint_acc->GetNode(k);
            Jacobian grad_qf_acc_sparse = RobotModel::DenseToSparse(grad_qf_acc.transpose());

            jac.row(GetRow(k, 2)) = grad_q0_acc_sparse * joint_pos->GetNodeJacobian(0)
                                    + grad_qf_acc_sparse * joint_pos->GetNodeJacobian(Nm1);
        }

        if (var_set == joint_vel->GetName()) {

            Jacobian S = S_mat_k_.sparseView();

            jac.row(GetRow(k, 1)) = S * joint_vel->GetNodeJacobian(k);
        }

        if (var_set == joint_acc->GetName()) {

            Jacobian S = S_mat_k_.sparseView();

            jac.row(GetRow(k, 2)) = S * joint_acc->GetNodeJacobian(k);
        }
    }

} /* namespace gambol */

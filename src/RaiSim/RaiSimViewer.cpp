#include <gambol/RaiSim/RaiSimViewer.h>

#include <gambol/RaiSim/raisimBasicImguiPanel.hpp>
#include <gambol/RaiSim/raisimKeyboardCallback.hpp>

namespace gambol {

    RaiSimViewer::RaiSimViewer() {

        auto vis = get_vis();

        vis->setWindowSize(800, 600);
        vis->setImguiSetupCallback(imguiSetupCallback);
        vis->setImguiRenderCallback(imguiRenderCallBack);
        vis->setSetUpCallback(setup_callback);
        vis->setKeyboardCallback(raisimKeyboardCallback);
        //vis->setAntiAliasing(2);
    }

    RaiSimViewer::RaiSimViewer(raisim::World& world) : RaiSimViewer() {

        auto vis = get_vis();
        vis->setWorld(&world);
        vis->initApp();
    }

    RaiSimViewer::RaiSimViewer(const RaiSimRobotModel::Ptr& model) : RaiSimViewer() {

        auto vis = get_vis();
        vis->setWorld(&model->GetWorld());
        vis->initApp();

        vis->createGraphicalObject(model->GetGround(), 20, "floor", "checkerboard_green");
        auto robot = model->GetSystem();
        auto robot_visual = vis->createGraphicalObject(robot, robot->getName());

        vis->getCameraMan()->getCamera()->setPosition(0, 2.0, 1.0);
        vis->getCameraMan()->getCamera()->pitch(Ogre::Radian(1.2));
        vis->select(robot_visual->at(0));
        vis->getCameraMan()->setYawPitchDist(Ogre::Radian(0.), Ogre::Radian(-1.5), 5);
    }

    RaiSimViewer::~RaiSimViewer() {

        auto vis = get_vis();
        vis->closeApp(); // Close just in case
    }

    raisim::OgreVis* RaiSimViewer::get_vis() {
        return raisim::OgreVis::get();
    }

    void RaiSimViewer::run() {
        get_vis()->run();
    }

    void RaiSimViewer::setup_callback() {
        auto vis = raisim::OgreVis::get();

        // Light
        vis->getLight()->setDiffuseColour(1, 1, 1);
        vis->getLight()->setCastShadows(true);
        vis->getLightNode()->setPosition(3, 3, 3);

        // Load  textures
        vis->addResourceDirectory(vis->getResourceDir() + "/material/gravel");
        vis->loadMaterialFile("gravel.material");

        vis->addResourceDirectory(vis->getResourceDir() + "/material/checkerboard");
        vis->loadMaterialFile("checkerboard.material");

        // Shadow setting
        vis->getSceneManager()->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_ADDITIVE);
        vis->getSceneManager()->setShadowTextureSettings(2048, 3);

        // Scale related settings! Please adapt it depending on your map size
        // Beyond this distance, shadow disappears:
        vis->getSceneManager()->setShadowFarDistance(60);
        // Size of contact points and contact forces:
        vis->setContactVisObjectSize(0.1, 3.0);
        // speed of camera motion in free-look mode
        vis->getCameraMan()->setTopSpeed(5);

        // Background
        Ogre::Quaternion quat;
        quat.FromAngleAxis(Ogre::Radian(M_PI_2), {1., 0, 0});
        vis->getSceneManager()->setSkyBox(true,
                                          "white",
                                          500,
                                          true,
                                          quat,
                                          Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME);
    }
}
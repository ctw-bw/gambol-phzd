#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <gambol/Robots/Robot.h>
#include <gambol/Robots/MuJoCoRobotModel.h>
#include <gambol/Robots/RaiSimRobotModel.h>
#include <gambol/Generators/GaitGeneratorMonoped.h>
#include <gambol/Generators/GaitGeneratorBiped.h>
#include <gambol/Generators/GaitGeneratorBipedFeet.h>
#include <gambol/Generators/GaitGeneratorBipedFeet3D.h>
#include <gambol/Generators/GaitGeneratorQuadruped.h>
#include <gambol/Generators/GaitGeneratorSnake.h>
#include <gambol/Generators/GaitGeneratorBipedArms.h>

namespace gambol {

    /**
     * Append one std::vector to another
     */
    template<typename T, typename A>
    void vector_append(std::vector<T, A>& v1, const std::vector<T, A>& v2) {
        v1.insert(v1.end(), v2.begin(), v2.end());
    }

    /**
     * Default constructor
     */
    Robot::Robot() {
        num_ee_ = 0;
        size_u_ = 0;
        size_q_ = 0;
        size_dq_ = 0;
        initial_zpos_ = false;
    }

    /**
     * Constructor for pre-defined robots
     */
    Robot::Robot(RobotName robot) {
        switch (robot) {
            case Monoped:
                MakeMonoped();
                break;
            case MonopedFoot:
                MakeMonopedFoot();
                break;
            case Monoped3D:
                MakeMonoped3D();
                break;
            case Monoped3DEuler:
                MakeMonoped3DEuler();
                break;
            case Biped:
                MakeBiped();
                break;
            case Biped3D:
                MakeBiped3D();
                break;
            case BipedFeet:
                MakeBipedFeet();
                break;
            case BipedFeet3D:
                MakeBipedFeet3D();
                break;
            case BipedArms3D:
                MakeBipedArms3D();
                break;
            case ExoSkeleton:
                MakeExoSkeleton();
                break;
            case Quadruped:
                MakeQuadruped();
                break;
            case Quadruped3D:
                MakeQuadruped3D();
                break;
            case Block:
                MakeBlock();
                break;
            case CartPole:
                MakeCartPole();
                break;
            case SnakeBot:
                MakeSnakeBot();
                break;
            case RaiSimBlock:
                MakeRaiSimBlock();
                break;
            case RaiSimCartPole:
                MakeRaiSimCartPole();
                break;
            case RaiSimMonoped:
                MakeRaiSimMonoped();
                break;
            case RaiSimBiped:
                MakeRaiSimBiped();
                break;
            case RaiSimBiped3D:
                MakeRaiSimBiped3D();
                break;
            case RaiSimWE2:
                MakeRaiSimWE2();
                break;
            default:
                assert(false && "This robot is not pre-defined");
        }
    }

    /**
     * Read joint limits from MuJoCo model
     *
     * Set both limits and bounds.
     */
    void Robot::JointLimitsFromMuJoCo(const MuJoCoModel& model, Limits& limits,
                                      Bounds& bounds) {
        limits = model.get_joint_limits();

        for (uint i = 0; i < limits.size(); i++) {
            const auto& lim = limits[i];

            if (lim.first > -0.5e19 && lim.second < 0.5e19)
                bounds.push_back(i);
        }
    }

    /**
     * Read torque limits from MuJoCo model
     *
     * Set both limits and bounds.
     */
    void Robot::TorqueLimitsFromMuJoCo(const MuJoCoModel& model, Limits& limits,
                                       Bounds& bounds) {
        limits = model.get_torque_limits();

        for (unsigned int i = 0; i < limits.size(); i++) {
            const auto& lim = limits[i];

            if (lim.first > -0.5e19 && lim.second < 0.5e19)
                bounds.push_back(i);
        }
    }

    /**
     * Create monoped robot
     */
    void Robot::MakeMonoped() {
        num_ee_ = 1;

        // Joints limits
        joint_pos_limits_ = {{-100.0,      100.0},
                             {-0.5,        100.0},
                             {-M_PI_2,
                                       M_PI_2},
                             {-M_PI_2, M_PI_2},
                             {-M_PI * 0.9, -0.05}};

        bound_joint_pos_limits_ = {1, 3, 4};

        // Torque limits
        const double max_torque = 250.0;
        torque_limits_ =
                {{-max_torque, max_torque},
                 {-max_torque, max_torque}};
        bound_torque_limits_ = {0, 1};

        size_q_ = joint_pos_limits_.size(); // x_b, z_b, q1, q2, q3
        size_u_ = torque_limits_.size(); // tau1, tau2
        size_dq_ = size_q_;

        // Joint position
        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << 0.0, -0.11, 0.2, 0.2, -1.0;

        bound_initial_joint_pos_ = {0, 2, 3, 4};
        //bound_initial_joint_vel_ = { 0, 1, 2, 3, 4 };
        bound_initial_joint_vel_ = {};

        // Gait
        generator_ = std::make_shared<GaitGeneratorMonoped>();

        std::vector<std::string> ee_names = {"link2"};
        robot_model_ = std::make_shared<MuJoCoRobotModel>("resources/monoped.xml",
                                                          size_q_, size_dq_, size_u_, ee_names);
    }

    /**
     * Create monoped robot
     *
     * Root node is at the foot
     */
    void Robot::MakeMonopedFoot() {
        num_ee_ = 1;

        // Joints limits
        joint_pos_limits_ = {{-100.0, 100.0},
                             {-100.0, 100.0},
                             {-M_PI_2,
                                       M_PI_2},
                             {0.05,   0.75 * M_PI},
                             {-M_PI_2, M_PI_2}};

        bound_joint_pos_limits_ = {2, 3, 4};

        // Torque limits
        const double max_torque = 100.0;
        torque_limits_ =
                {{-max_torque, max_torque},
                 {-max_torque, max_torque}};
        bound_torque_limits_ = {0, 1};

        size_q_ = joint_pos_limits_.size(); // x_b, z_b, q1, q2, q3
        size_u_ = torque_limits_.size(); // tau1, tau2
        size_dq_ = size_q_;

        // Joint position
        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << 0.0, 0.0, -0.5, 1.0, -0.3;

        bound_initial_joint_pos_ = {0, 2, 3, 4};
        bound_initial_joint_vel_ = {0, 1};

        // Gait
        generator_ = std::make_shared<GaitGeneratorMonoped>();
        ee_phase_durations_.push_back({0.4, 0.2, 0.4, 0.2, 0.2});
        initial_contact_.push_back(true); // Start on the ground

        std::vector<std::string> ee_names = {"link2"};
        robot_model_ = std::make_shared<MuJoCoRobotModel>(
                "resources/monoped_foot.xml", size_q_, size_dq_, size_u_,
                ee_names);
    }

    /**
     * Make monoped robot
     *
     * Model is in 3D
     */
    void Robot::MakeMonoped3D() {
        num_ee_ = 1;

        // Joints limits
        joint_pos_limits_ = {
                // Free joint:
                {-100.0,      100.0},
                {-100.0,      100.0},
                {-100.0,      100.0},
                {-1.0,        1.0},
                {-1.0,        1.0},
                {-1.0,        1.0},
                {-1.0,        1.0},
                // Regular joints:
                {-M_PI_2, M_PI_2},
                {-M_PI_2, M_PI_2},
                {-M_PI * 0.9, -0.05}};

        bound_joint_pos_limits_ = { /*0, 1, 2,*/3, 4, 5, 6, 7, 8, 9};

        // Torque limits
        const double max_torque = 100.0;
        torque_limits_ = {{-max_torque, max_torque},
                          {-max_torque, max_torque},
                          {-max_torque, max_torque}};
        bound_torque_limits_ = {0, 1, 2};

        size_q_ = joint_pos_limits_.size();
        size_u_ = torque_limits_.size();
        size_dq_ = size_q_ - 1; // Contains one quaternion

        // Joint position
        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << 0.0, 0.0, 0.9, // Position
                MuJoCo::euler_to_quaternion(0.0, 0.3, 0.0), // Quaternion
                0.25 - 0.3, 0.0, -0.5; // Joints

        bound_initial_joint_pos_ = {0, 1, 3, 4, 5, 6, 7, 8, 9};
        bound_initial_joint_vel_ = {}; // qvel != qpos

        // Gait
        generator_ = std::make_shared<GaitGeneratorMonoped>();

        std::vector<std::string> ee_names = {"link2"};
        auto mj_model = std::make_shared<MuJoCoRobotModel>(
                "resources/monoped_3d.xml", size_q_, size_dq_, size_u_, ee_names);

        //mj_model->SetBaseName("base");

        robot_model_ = mj_model;
    }

    /**
     * Make monoped robot with Euler angles
     *
     * Model is in 3D and does not contain quaternions
     */
    void Robot::MakeMonoped3DEuler() {
        num_ee_ = 1;

        // Joints limits
        joint_pos_limits_ = {
                // Free joint:
                {-100.0,      100.0},
                {-100.0,      100.0},
                {-100.0,      100.0},
                {0.0,         0.0},
                {-10.0,       10.0},
                {0.0,         0.0},
                // Regular joints:
                {-M_PI_2, M_PI_2},
                {-M_PI_2, M_PI_2},
                {-M_PI * 0.9, -0.05}};

        bound_joint_pos_limits_ = { /*0, 1, 2, 3, 4,  5,*/6, 7, 8};

        // Torque limits
        const double max_torque = 100.0;
        torque_limits_ = {{-max_torque, max_torque},
                          {-max_torque, max_torque},
                          {-max_torque, max_torque}};
        bound_torque_limits_ = {0, 1, 2};

        size_q_ = joint_pos_limits_.size();
        size_u_ = torque_limits_.size();
        size_dq_ = size_q_;

        // Joint position
        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << 0.0, 0.0, 0.0, // Position
                0.0, 0.0, 0.0, // Euler angle
                0.25, 0.0, -0.5; // Joints

        bound_initial_joint_pos_ = {0, 1, 3, 4, 5, 6, 7, 8};
        //bound_initial_joint_vel_ = { 0, 1, 2, 6, 7, 8 };
        bound_initial_joint_vel_ = {};

        // Gait
        generator_ = std::make_shared<GaitGeneratorMonoped>();
        //ee_phase_durations_.push_back( { 0.4, 0.2, 0.4, 0.2, 0.2 });
        //initial_contact_.push_back(true); // Start on the ground

        std::vector<std::string> ee_names = {"link2"};
        robot_model_ = std::make_shared<MuJoCoRobotModel>(
                "resources/monoped_3d_euler.xml", size_q_, size_dq_, size_u_,
                ee_names);
    }

    /**
     * Create biped robot model
     */
    void Robot::MakeBiped() {
        num_ee_ = 2;

        // Joints limits
        joint_pos_limits_ = {{-100.0, 100.0},
                             {-0.2,   0.2},
                             {-M_PI_2,
                                       M_PI_2},
                             {-M_PI_2, M_PI_2},
                             {0.01,   0.9 * M_PI},
                             {-M_PI_2, M_PI_2},
                             {
                              0.01,   0.9 * M_PI}};

        bound_joint_pos_limits_ = { /*0, 1, 2,*/3, 4, 5, 6};

        // Torque limits
        const double max_torque = 75.0;
        torque_limits_ = {{-max_torque, max_torque},
                          {-max_torque, max_torque},
                          {-max_torque, max_torque},
                          {-max_torque, max_torque}};
        bound_torque_limits_ = {0, 1, 2, 3};

        size_q_ = joint_pos_limits_.size(); // x_b, z_b, q_world, q_lu, q_ll, q_ru, q_rl
        size_u_ = torque_limits_.size(); // tau_l1, tau_l2, tau_r1, tau_r2
        size_dq_ = size_q_;

        // Joint position
        basic_joint_pos_ = VectorXd::Zero(size_q_);
        const double lean = 0.0;
        basic_joint_pos_ << 0.0, 0.0, lean, -0.1 + lean, 0.2, -0.1 + lean, 0.2;

        bound_initial_joint_pos_ = {0, /*1,*/2, 3, 4, 5, 6};
        //bound_initial_joint_pos_ = { 0 };
        bound_initial_joint_vel_ = {0, 1, 2, 3, 4, 5, 6};
        //bound_initial_joint_vel_ = { };

        bound_symmetry_joint_pos_ = { /*0,*/1, 2, 3, 4, 5, 6};

        // Gait
        generator_ = std::make_shared<GaitGeneratorBiped>();

        std::vector<std::string> ee_names = {"left_lower_leg", "right_lower_leg"};
        auto mj_model = std::make_shared<MuJoCoRobotModel>(
                "resources/five_link_biped.xml", size_q_, size_dq_, size_u_,
                ee_names);

        //mj_model->SetBaseName("base");

        robot_model_ = mj_model;
    }

    /**
     * Create biped robot model
     */
    void Robot::MakeBiped3D() {
        num_ee_ = 2;

        // Joints limits
        joint_pos_limits_ = {
                // Free joint:
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                // Regular joints:
                {-M_PI_2, M_PI_2},
                {-0.2,   0.2},
                {-0.1,   0.1},
                {0.05,    M_PI
                          * 0.9}, // leg swing, leg lift, leg twist, knee
                {-M_PI_2, M_PI_2},
                {-0.2,   0.2},
                {-0.1,   0.1},
                {0.05,    M_PI
                          * 0.9}}; // leg swing, leg lift, leg twist, knee

        bound_joint_pos_limits_ = { /*0, 1, 2,*/3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                                14};

        // Torque limits
        const double max_torque = 100.0;

        size_u_ = 8;

        for (int i = 0; i < size_u_; i++) {
            torque_limits_.push_back({-max_torque, max_torque});
            bound_torque_limits_.push_back(i);
        }

        size_q_ = joint_pos_limits_.size();
        size_dq_ = size_q_ - 1; // Contains one quaternion

        // Joint position
        Vector3d pos(0.0, 0.0, 0.75);
        Vector4d quat = MuJoCo::euler_to_quaternion(0.0, 0.1, 0.0);
        Vector4d leg_pos(-0.25 - 0.3, 0.0, 0.0, 0.5);

        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << pos, quat, leg_pos, leg_pos;

        bound_initial_joint_pos_ = {0, 1, /* 2, */3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
                                    13, 14};
        //bound_initial_joint_pos_ = { 0, /*1, 2,*/ 3, 4, 5, 6 };
        bound_initial_joint_vel_ = {}; // qvel != qpos

        bound_symmetry_joint_pos_ = { /*0,*/1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
                                            13, 14};

        // Gait
        generator_ = std::make_shared<GaitGeneratorBiped>();

        std::vector<std::string> ee_names = {"left_lower_leg", "right_lower_leg"};
        auto mj_model = std::make_shared<MuJoCoRobotModel>(
                "resources/five_link_biped_3d.xml", size_q_, size_dq_, size_u_,
                ee_names);

        //mj_model->SetBaseName("base");

        robot_model_ = mj_model;
    }

    /**
     * Create biped robot with feet
     */
    void Robot::MakeBipedFeet() {
        num_ee_ = 4;

        const std::string file = "resources/seven_link_biped_toes.xml";
        MuJoCoModel mj_model(file);

        // Joints limits
        JointLimitsFromMuJoCo(mj_model, joint_pos_limits_, bound_joint_pos_limits_);

        // Torque limits
        TorqueLimitsFromMuJoCo(mj_model, torque_limits_, bound_torque_limits_);

        size_q_ = joint_pos_limits_.size(); // x_b, z_b, q_world, q_lu, q_ll, q_la, q_ru, q_rl, q_ra
        size_u_ = torque_limits_.size();
        size_dq_ = size_q_;

        // Joint position
        basic_joint_pos_ = VectorXd::Zero(size_q_);
        const double lean = 0.2;
        basic_joint_pos_ << 0.0, 0.0, lean, -0.1 - lean, 0.2, -0.1, -0.1 - lean, 0.2, -0.1;

        bound_initial_joint_pos_ = {0, /*1,*/ 2, 3, 4, 5, 6, 7, 8};
        bound_initial_joint_vel_ = {0, 1, 2, 3, 4, 5, 6, 7, 8};

        // Gait
        generator_ = std::make_shared<GaitGeneratorBipedFeet>();

        std::vector<std::string> ee_names = {"left_heel", "left_toes",
                                             "right_heel", "right_toes"};
        robot_model_ = std::make_shared<MuJoCoRobotModel>(file, size_q_, size_dq_,
                                                          size_u_, ee_names);
    }

    /**
     * Create biped robot with feet in 3D
     */
    void Robot::MakeBipedFeet3D() {
        num_ee_ = 6; // Triangular feet

        const std::string file = "resources/seven_link_biped_toes_3d.xml";
        MuJoCoModel mj_model(file);

        // Joints limits
        JointLimitsFromMuJoCo(mj_model, joint_pos_limits_, bound_joint_pos_limits_);

        // Torque limits
        TorqueLimitsFromMuJoCo(mj_model, torque_limits_, bound_torque_limits_);

        size_q_ = joint_pos_limits_.size();
        size_dq_ = size_q_ - 1;
        size_u_ = torque_limits_.size();

        // Joint position
        Vector3d pos(0.0, 0.0, 0.81);
        const double lean = 0.1;
        Vector4d quat = MuJoCo::euler_to_quaternion(0.0, lean, 0.0);
        VectorXd leg_pos(6);
        leg_pos << -0.1 - lean, 0.0, 0.0, 0.2, -0.1, 0.0;

        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << pos, quat, leg_pos, leg_pos;

        //bound_initial_joint_pos_ = { 0, 1, /* 2, */3, 4, 5, 6, 7, 8, 9, /*10, 11, 12, */ 13, 14, 15, /* 16, 17, 18*/};
        bound_initial_joint_pos_ = {0, 1, /* 2, */3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
                                    13, 14, 15, 16, 17, 18};

        //bound_initial_joint_vel_ = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 }; // qvel != qpos
        bound_initial_joint_vel_ = {};

        bound_symmetry_joint_pos_ = { /*0,*/1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
                                            13, 14, 15, 16, 17, 18};

        // Gait
        generator_ = std::make_shared<GaitGeneratorBipedFeet3D>();

        std::vector<std::string> ee_names = {"left_heel", "left_toes_l",
                                             "left_toes_r", "right_heel", "right_toes_r", "right_toes_l"};
        robot_model_ = std::make_shared<MuJoCoRobotModel>(
                file, size_q_, size_dq_,
                size_u_, ee_names);
    }

    /**
     * Create biped robot model with pin feet and arms in 3D
     */
    void Robot::MakeBipedArms3D() {
        num_ee_ = 4;

        // Joints limits

        std::vector<std::pair<double, double>> leg_limits = {{-1.0,  1.0},
                                                             {
                                                              -0.15, 1.0},
                                                             {-0.8,  0.8}, // Hip
                                                             {0.05, M_PI * 0.75} // Knee
        };
        std::vector<std::pair<double, double>> arm_limits = {{-4.0 * M_PI,  4.0
                                                                            * M_PI},
                                                             {-0.15,   M_PI},
                                                             {-M_PI_2, M_PI_2}, // Shoulder
                                                             {-M_PI * 0.75, -0.05} // Elbow
        };

        joint_pos_limits_ = {
                // Free joint:
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0}};

        vector_append(joint_pos_limits_, leg_limits);
        vector_append(joint_pos_limits_, leg_limits);
        vector_append(joint_pos_limits_, arm_limits);
        vector_append(joint_pos_limits_, arm_limits);

        bound_joint_pos_limits_ = { /*0, 1, 2,*/3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                                14, 15, 16, 17, 18, 19, 20, 21, 22};
        //bound_joint_pos_limits_ = { /*0, 1, 2,*/3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };

        // Torque limits
        const double max_torque = 200.0;

        size_u_ = 16;

        for (int i = 0; i < size_u_; i++) {
            torque_limits_.push_back({-max_torque, max_torque});
            bound_torque_limits_.push_back(i);
        }

        size_q_ = joint_pos_limits_.size();
        size_dq_ = size_q_ - 1; // Contains one quaternion

        // Joint position
        Vector3d pos(0.0, 0.0, 0.80);
        Vector4d quat = MuJoCo::euler_to_quaternion(0.0, 0.1, 0.0);
        Vector4d leg_pos(-0.025 - 0.1, 0.0, 0.0, 0.05);
        Vector4d arm_pos(0.0, 0.3, -0.3, -0.6);

        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << pos, quat, leg_pos, leg_pos, arm_pos, arm_pos;

        bound_initial_joint_pos_ = {0, 1, /* 2, */3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
                                    13, 14, 15, 16, 17, 18, 19, 20, 21, 22};
        //bound_initial_joint_pos_ = { };
        //bound_initial_joint_vel_ = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 };
        bound_initial_joint_vel_ = {}; // qvel != qpos

        // Gait
        generator_ = std::make_shared<GaitGeneratorBipedArms>();

        std::vector<std::string> ee_names = {"left_lower_leg", "right_lower_leg",
                                             "left_lower_arm", "right_lower_arm"};
        auto mj_model = std::make_shared<MuJoCoRobotModel>(
                "resources/five_link_biped_hands_3d.xml", size_q_, size_dq_,
                size_u_, ee_names);

        //mj_model->SetBaseName("base");

        robot_model_ = mj_model;
    }

    /**
     * Create robot of the BME exoskeleton
     */
    void Robot::MakeExoSkeleton() {
        num_ee_ = 6; // Triangular feet

        // Joints limits
        std::vector<std::pair<double, double>> leg_limits = {{-0.5,  0.5},
                                                             {
                                                              -0.05, 0.5}, // Hip
                                                             {0.01,  0.8 * M_PI}, // Knee
                                                             {-0.8,  0.8},
                                                             {-0.8,  0.8} // Ankle
        };

        joint_pos_limits_ = {
                // Free joint:
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0}};
        // Regular joints:
        for (int i = 0; i < 2; i++) {
            joint_pos_limits_.insert(joint_pos_limits_.end(), leg_limits.begin(),
                                     leg_limits.end());
        }

        bound_joint_pos_limits_ = { /*0, 1, 2,*/3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                                14, 15, 16};

        // Torque limits
        size_u_ = 8;
        size_q_ = joint_pos_limits_.size();
        size_dq_ = size_q_ - 1; // One quaternion

        const double max_torque = 150.0;

        for (int i = 0; i < size_u_; i++) {
            torque_limits_.push_back({-max_torque, max_torque});
            bound_torque_limits_.push_back(i);
        }

        // Joint position
        Vector3d pos(0.0, 0.0, 0.81);
        Vector4d quat = MuJoCo::euler_to_quaternion(0.0, 0.0, 0.0);
        VectorXd leg_pos(5);
        leg_pos << -0.1, 0.0, 0.2, 0.0, -0.1;

        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << pos, quat, leg_pos, leg_pos;

        bound_initial_joint_pos_ = {0, 1, /* 2, */3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
                                    13, 14, 15, 16};
        //bound_initial_joint_pos_ = { 0, 1, /* 2, *//*3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,*/14, /*15, 16*/};
        // Ankles are left free

        bound_initial_joint_vel_ = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                    14, 15}; // qvel != qpos
        //bound_initial_joint_vel_ = { };

        bound_symmetry_joint_pos_ = { /*0,*/1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
                                            13, 14, 15, 16};

        // Gait
        generator_ = std::make_shared<GaitGeneratorBipedFeet3D>();

        std::vector<std::string> ee_names = {"left_heel", "left_toes_l",
                                             "left_toes_r", "right_heel", "right_toes_r", "right_toes_l"};
        robot_model_ = std::make_shared<MuJoCoRobotModel>(
                "resources/exoskeleton.xml", size_q_, size_dq_, size_u_, ee_names);
    }

    /**
     * Create quadruped in 2D robot model
     */
    void Robot::MakeQuadruped() {
        num_ee_ = 4;

        const std::string file = "resources/quadruped.xml";
        MuJoCoModel mj_model(file);

        // Joints limits
        JointLimitsFromMuJoCo(mj_model, joint_pos_limits_, bound_joint_pos_limits_);

        // Torque limits
        TorqueLimitsFromMuJoCo(mj_model, torque_limits_, bound_torque_limits_);

        size_q_ = joint_pos_limits_.size();
        size_dq_ = size_q_;
        size_u_ = torque_limits_.size();

        // Joint position
        VectorXd leg_pos(2);
        leg_pos << 0.25, -0.5;

        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << 0.0, -0.03, 0.0, leg_pos, leg_pos, leg_pos, leg_pos;

        bound_initial_joint_pos_ = {0, /*1,*/2, 3, 4, 5, 6, 7, 8, 9, 10};
        //bound_initial_joint_pos_ = { 0, /*1, 2,*/ 3, 4, 5, 6 };
        bound_initial_joint_vel_ = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; // qvel != qpos

        // Gait
        generator_ = std::make_shared<GaitGeneratorQuadruped>();

        std::vector<std::string> ee_names = {"lf_lower_leg", "rf_lower_leg",
                                             "lr_lower_leg", "rr_lower_leg"};
        robot_model_ = std::make_shared<MuJoCoRobotModel>(file, size_q_, size_dq_,
                                                          size_u_, ee_names);
    }

    /**
     * Create quadruped in 3D robot model
     */
    void Robot::MakeQuadruped3D() {
        num_ee_ = 4;

        // Joints limits
        std::vector<std::pair<double, double>> leg_limits = {{-0.2,        0.2},
                                                             {-0.4,
                                                                           1.2},
                                                             {-0.9 * M_PI, -0.05}};

        joint_pos_limits_ = {
                // Free joint:
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0}};
        // Regular joints:
        for (int i = 0; i < num_ee_; i++) {
            joint_pos_limits_.insert(joint_pos_limits_.end(), leg_limits.begin(),
                                     leg_limits.end());
        }

        bound_joint_pos_limits_ = { /*0, 1, 2,*/3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                                14, 15, 16, 17, 18};

        // Torque limits
        const double max_torque = 100.0;

        size_u_ = 12;

        for (int i = 0; i < size_u_; i++) {
            torque_limits_.push_back({-max_torque, max_torque});
            bound_torque_limits_.push_back(i);
        }

        size_q_ = joint_pos_limits_.size();
        size_dq_ = size_q_ - 1; // Contains one quaternion

        // Joint position
        Vector3d pos(0.0, 0.0, 0.5);
        Vector4d quat = MuJoCo::euler_to_quaternion(0.0, 0.0, 0.0);
        Vector3d leg_pos(0.0, 0.5, -1.0);

        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << pos, quat, leg_pos, leg_pos, leg_pos, leg_pos;

        bound_initial_joint_pos_ = {0, 1, /* 2, */3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
                                    13, 14, 15, 16, 17, 18};
        //bound_initial_joint_pos_ = { 0, /*1, 2,*/ 3, 4, 5, 6 };
        bound_initial_joint_vel_ = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
                                    13, 14, 15, 16, 17}; // qvel != qpos

        // Gait
        generator_ = std::make_shared<GaitGeneratorQuadruped>();

        std::vector<std::string> ee_names = {"lf_lower_leg", "rf_lower_leg",
                                             "lr_lower_leg", "rr_lower_leg"};
        auto mj_model = std::make_shared<MuJoCoRobotModel>(
                "resources/quadruped_3d.xml", size_q_, size_dq_, size_u_,
                ee_names);

        //mj_model->SetBaseName("base");

        robot_model_ = mj_model;
    }

    /**
     * Make simple legless block to test dynamics
     *
     * Model is in 3D
     */
    void Robot::MakeBlock() {
        /**
         * Just a single block has no errors at all, but as soon as another body is attached to it there are
         */

        num_ee_ = 1;

        // Joints limits
        joint_pos_limits_ = {
                // Free joint:
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-M_PI,
                        M_PI}};

        bound_joint_pos_limits_ = { /*0, 1, 2,*/3, 4, 5, 6, /*7*/};

        // Torque limits
        const double max_torque = 100.0;
        torque_limits_ = {{-max_torque, max_torque}};
        bound_torque_limits_ = {0};

        size_q_ = joint_pos_limits_.size();
        size_u_ = torque_limits_.size();
        size_dq_ = size_q_ - 1; // Contains one quaternion

        // Joint position
        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << 0.0, 0.0, 2.0, // Position
                MuJoCo::euler_to_quaternion(1.74, -2.16, 0.31), // Quaternion
                0.0;

        bound_initial_joint_pos_ = { /*0, 1, 2,*/3, 4, 5, /*6*/};
        bound_initial_joint_vel_ = {}; // qvel != qpos

        // Gait
        generator_ = std::make_shared<GaitGeneratorMonoped>();
        //ee_phase_durations_.push_back( { 0.4, 0.2, 0.4, 0.2, 0.2 });
        //initial_contact_.push_back(true); // Start on the ground

        std::vector<std::string> ee_names = {"leg"};
        robot_model_ = std::make_shared<MuJoCoRobotModel>("resources/block.xml",
                                                          size_q_, size_dq_, size_u_, ee_names);
    }

    /**
     * Make a 2D pendulum on a cart
     */
    void Robot::MakeCartPole() {
        num_ee_ = 0;

        // Joints limits
        joint_pos_limits_ = {
                // Free joint:
                {-1.0,        1.0},
                {-2.0 * M_PI, 2.0 * M_PI}};

        bound_joint_pos_limits_ = {0, 1};

        // Torque limits
        const double max_torque = 1000.0;
        torque_limits_ = {{-max_torque, max_torque}};
        bound_torque_limits_ = {0};

        size_q_ = joint_pos_limits_.size();
        size_dq_ = size_q_;
        size_u_ = torque_limits_.size();

        // Joint position
        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << 0.0, 0.0;

        bound_initial_joint_pos_ = {0, 1};
        bound_initial_joint_vel_ = {0, 1};

        // Gait
        generator_ = std::make_shared<GaitGeneratorMonoped>();

        std::vector<std::string> ee_names = {"pole"};
        robot_model_ = std::make_shared<MuJoCoRobotModel>("resources/cartpole.xml",
                                                          size_q_, size_dq_, size_u_, ee_names);
    }

    /**
     * Create biped robot model
     */
    void Robot::MakeSnakeBot() {
        num_ee_ = 3;

        // Joints limits
        joint_pos_limits_ = {{-100.0, 100.0},
                             {-0.1,   0.5},
                             {0.0, M_PI}, //
                             {-M_PI,  0.0},
                             {0.0, M_PI},
                             {-M_PI,  0.0}};

        bound_joint_pos_limits_ = {1, 2, 3, 4, 5};

        // Torque limits
        const double max_torque = 300.0;
        torque_limits_ = {{-max_torque, max_torque},
                          {-max_torque, max_torque},
                          {-max_torque, max_torque}};
        bound_torque_limits_ = {0, 1, 2};

        size_q_ = joint_pos_limits_.size();
        size_u_ = torque_limits_.size();
        size_dq_ = size_q_;

        // Joint position
        basic_joint_pos_ = VectorXd::Zero(size_q_);
        const double a = M_PI_4; // Curl angle
        basic_joint_pos_ << 0.0, 0.0, a, -2.0 * a, 2.0 * a, -2.0 * a;

        bound_initial_joint_pos_ = {0, /*1,*/2, 3, 4, 5};
        bound_initial_joint_vel_ = {0, 1, 2, 3, 4, 5};

        bound_symmetry_joint_pos_ = { /*0,*/1, 2, 3, 4, 5, 6};

        // Gait
        generator_ = std::make_shared<GaitGeneratorSnake>();

        std::vector<std::string> ee_names = {"link4_tip", "link3", "link1"};
        auto mj_model = std::make_shared<MuJoCoRobotModel>(
                "resources/snake_bot.xml", size_q_, size_dq_, size_u_, ee_names);

        //mj_model->SetBaseName("base");

        robot_model_ = mj_model;
    }

    /**
     * Make simple legless block to test dynamics
     *
     * This model is simulated by RaiSim instead of MuJoCo!
     * Model is in 3D
     */
    void Robot::MakeRaiSimBlock() {

        num_ee_ = 1;

        // Joints limits
        joint_pos_limits_ = {
                // Free joint:
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0}};

        bound_joint_pos_limits_ = { /*0, 1, 2,3, 4, 5, 6, 7*/};

        // Torque limits
        const double max_torque = 100.0;
        torque_limits_ = {{-max_torque, max_torque},
                          {-max_torque, max_torque},
                          {-max_torque, max_torque},
                          {-max_torque, max_torque},
                          {-max_torque, max_torque},
                          {-max_torque, max_torque}};
        bound_torque_limits_ = {0, 1, 2, 3, 4, 5};

        size_q_ = joint_pos_limits_.size();
        size_u_ = 6;
        size_dq_ = size_q_ - 1; // Contains one quaternion

        // Joint position
        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << 0.0, 0.0, 2.0, // Position
                1.0, 0.0, 0.0, 0.0; // Quaternion

        bound_initial_joint_pos_ = {0, 1, 2, 3, 4, 5, 6};
        bound_initial_joint_vel_ = {0, 1, 2, 3, 4, 5}; // qvel != qpos

        // Gait
        generator_ = std::make_shared<GaitGeneratorMonoped>();
        //ee_phase_durations_.push_back( { 0.4, 0.2, 0.4, 0.2, 0.2 });
        //initial_contact_.push_back(true); // Start on the ground

        std::vector<std::string> ee_names = {"link_ee_joint"};
        auto robot_model = std::make_shared<RaiSimRobotModel>("resources/block.urdf",
                                                              size_q_, size_dq_, size_u_, ee_names,
                                                              Eigen::MatrixXd::Identity(size_dq_, size_u_));
        robot_model_ = robot_model;

        robot_model->GetWorld().setGravity({0.0, 0.0, 0.0}); // Disable gravity
    }

    /**
     * Make a 2D pendulum on a cart, made in RaiSim
     */
    void Robot::MakeRaiSimCartPole() {

        num_ee_ = 0;

        // Joints limits
        joint_pos_limits_ = {
                // Free joint:
                {-1.0,        1.0},
                {-2.0 * M_PI, 2.0 * M_PI}};

        bound_joint_pos_limits_ = {0, 1};

        // Torque limits
        const double max_torque = 1000.0;
        torque_limits_ = {{-max_torque, max_torque}};
        bound_torque_limits_ = {0};

        size_q_ = joint_pos_limits_.size();
        size_dq_ = size_q_;
        size_u_ = torque_limits_.size();

        // Joint position
        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << 0.0, 0.0;

        bound_initial_joint_pos_ = {0, 1};
        bound_initial_joint_vel_ = {0, 1};

        // Gait
        generator_ = std::make_shared<GaitGeneratorMonoped>();

        std::vector<std::string> ee_names = {"ee_joint"};
        Eigen::MatrixXd S(size_dq_, size_u_);
        S << 1.0, 0.0;
        robot_model_ = std::make_shared<RaiSimRobotModel>("resources/cartpole.urdf",
                                                          size_q_, size_dq_, size_u_, ee_names, S);
    }

    /**
     * Make a monoped in 2D using RaiSim
     */
    void Robot::MakeRaiSimMonoped() {

        num_ee_ = 1;

        // Joints limits
        joint_pos_limits_ = {{-100.0,      100.0},
                             {0.8,         100.0},
                             {-M_PI_2, M_PI_2},
                             {-M_PI_2, M_PI_2},
                             {-M_PI * 0.9, -0.05}};

        bound_joint_pos_limits_ = {1, 2, 3, 4};

        // Torque limits
        const double max_torque = 1000.0;
        torque_limits_ = {{-max_torque, max_torque},
                          {-max_torque, max_torque}};
        bound_torque_limits_ = {0, 1};

        size_q_ = joint_pos_limits_.size(); // x_b, z_b, q1, q2, q3
        size_u_ = torque_limits_.size(); // tau1, tau2
        size_dq_ = size_q_;

        // Joint position
        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << 0.0, 1.2, 0.2, 0.2, -1.0;

        bound_initial_joint_pos_ = {0, /*1, 2,*/ 3, 4};
        bound_initial_joint_vel_ = {0, 1, 2, 3, 4};

        // Gait
        generator_ = std::make_shared<GaitGeneratorMonoped>();

        std::vector<std::string> ee_names = {"link_ee_joint"};
        auto model = std::make_shared<RaiSimRobotModel>("resources/monoped.urdf",
                                                        size_q_, size_dq_, size_u_, ee_names);
        //raisim::World& world = model->GetWorld();
        //world.setERP(world.getTimeStep() * 100.0, world.getTimeStep() * 100.0);

        robot_model_ = model;
    }

    /**
     * Created five-link biped in 2D, modelled in RaiSim
     */
    void Robot::MakeRaiSimBiped() {

        num_ee_ = 2;
        size_q_ = 7;
        size_dq_ = size_q_;
        size_u_ = 4;

        std::vector<std::string> ee_names = {"left_ankle", "right_ankle"};
        auto model = std::make_shared<RaiSimRobotModel>(
                "resources/five_link_biped.urdf", size_q_, size_dq_, size_u_,
                ee_names);

        joint_pos_limits_ = model->GetJointLimits(false, &bound_joint_pos_limits_);
        torque_limits_ = model->GetActuatorLimits(&bound_torque_limits_);

        // Joint position
        basic_joint_pos_ = VectorXd::Zero(size_q_);
        const double lean = 0.0;
        basic_joint_pos_ << 0.0, 0.995, lean, -0.1 + lean, 0.2, -0.1 + lean, 0.2;

        //bound_initial_joint_pos_ = {0, /*1,*/ 2, 3, 4, 5, 6};
        bound_initial_joint_pos_ = { 0, /*1,2, 3, */ /*4, 5, 6*/ };
        //bound_initial_joint_vel_ = {0, 1, 2, 3, 4, 5, 6};
        bound_initial_joint_vel_ = {};

        // Symmetry
        S_q_f_ = Eigen::MatrixXd::Zero(size_q_ - 1, size_q_);
        S_q_0_ = S_q_f_;

        S_q_f_(0, 1) = 1.0; // y
        S_q_f_(1, 2) = 1.0; // angle
        S_q_f_(2, 5) = 1.0; // hip // Swap left/right leg
        S_q_f_(3, 6) = 1.0; // knee
        S_q_f_(4, 3) = 1.0; // hip
        S_q_f_(5, 4) = 1.0; // knee

        S_q_0_.block(0, 1, size_q_ - 1, size_q_ - 1).setIdentity();
        S_dq_0_ = S_q_0_;
        S_dq_f_ = S_q_f_;

        // Virtual constraint stuff
        J_hip_ = Eigen::MatrixXd(1, 7);
        J_hip_ << 0.0, 0.0, 0.0, 0.0, 0.0, 0.5 + 0.5, 0.5;

        H_ = Eigen::MatrixXd::Zero(4, size_q_);
        // All
        H_(0,2) = 1.0;
        H_(1,3) = 1.0;
        H_(2,4) = 1.0;
        H_(3,5) = 1.0;

        // Gait
        generator_ = std::make_shared<GaitGeneratorBiped>();

        robot_model_ = model;
    }

    /**
     * Create biped robot model in 3D, simulated by RaiSim
     */
    void Robot::MakeRaiSimBiped3D() {
        num_ee_ = 2;

        // Joints limits
        joint_pos_limits_ = {
                // Floating base
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                // Regular joints:
                {-M_PI_2, M_PI_2},
                {-0.2,   0.2},
                {0.0,    0.0},
                {0.05,    M_PI * 0.9}, // leg swing, leg lift, leg twist, knee
                {-M_PI_2, M_PI_2},
                {-0.2,   0.2},
                {0.0,    0.0},
                {0.05,    M_PI * 0.9}, // leg swing, leg lift, leg twist, knee
        };

        bound_joint_pos_limits_ = { /*0, 1, 2,*/3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                                14};

        // Torque limits
        const double max_torque = 500.0;

        size_u_ = 8;

        for (int i = 0; i < size_u_; i++) {
            torque_limits_.push_back({-max_torque, max_torque});
            bound_torque_limits_.push_back(i);
        }

        size_q_ = joint_pos_limits_.size();
        size_dq_ = size_q_ - 1; // Contains one quaternion

        // Joint position
        Vector3d pos(0.0, 0.0, 0.95);
        Vector4d quat = MuJoCo::euler_to_quaternion(0.0, 0.1, 0.0);
        Vector4d leg_pos(-0.25 - 0.2, 0.0, 0.0, 0.5);

        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << pos, quat, leg_pos, leg_pos;

        bound_initial_joint_pos_ = {0, 1, /* 2, */3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
                                    13, 14};
        //bound_initial_joint_pos_ = { 0, /*1, 2,*/ 3, 4, 5, 6 };
        bound_initial_joint_vel_ = {}; // qvel != qpos

        bound_symmetry_joint_pos_ = { /*0,*/1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
                                            13, 14};

        // Gait
        generator_ = std::make_shared<GaitGeneratorBiped>();

        std::vector<std::string> ee_names = {"left_ankle", "right_ankle"};
        robot_model_ = std::make_shared<RaiSimRobotModel>(
                "resources/five_link_biped_3d.urdf", size_q_, size_dq_, size_u_,
                ee_names);
    }

    /**
     * Create biped robot model in 3D, simulated by RaiSim
     */
    void Robot::MakeRaiSimWE2() {

        num_ee_ = 6; // One for each heel, two for each set of toes

        // Joints limits
        joint_pos_limits_ = {
                // Floating base
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-100.0, 100.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                {-1.0,   1.0},
                // Regular joints:
                {-0.2,   0.2},
                {-M_PI_2, M_PI_2},
                {0.05,    M_PI * 0.9},
                {-M_PI_4, M_PI_2},
                {-M_PI_4, M_PI_4},
                // lift (abduction), swing (flexion), knee (extension), ankle dip (plantarflexion), ankle roll (inversion)
                {-0.2,   0.2},
                {-M_PI_2, M_PI_2},
                {0.05,    M_PI * 0.9},
                {-M_PI_2, M_PI_2},
                {-M_PI_4, M_PI_4}, // Same (symmetric)
        };

        bound_joint_pos_limits_ = { /*0, 1, 2,*/3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};

        // Torque limits
        const double max_torque = 500.0;

        size_u_ = 10;

        for (int i = 0; i < size_u_; i++) {
            torque_limits_.push_back({-max_torque, max_torque});
            bound_torque_limits_.push_back(i);
        }

        size_q_ = joint_pos_limits_.size();
        size_dq_ = size_q_ - 1; // Contains one quaternion

        // Joint position
        Vector3d pos(0.0, 0.0, 1.00);
        const float lean = 0.3;
        Vector4d quat = MuJoCo::euler_to_quaternion(0.0, lean, 0.0);
        VectorXd leg_pos(5);
        leg_pos << 0.0, 0.25 + lean, 0.5, -0.25, 0.0;

        basic_joint_pos_ = VectorXd::Zero(size_q_);
        basic_joint_pos_ << pos, quat, leg_pos, leg_pos;

        bound_initial_joint_pos_ = {0, 1, /* 2, */3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
        bound_initial_joint_vel_ = {}; // qvel != qpos

        bound_symmetry_joint_pos_ = { /*0,*/1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};

        // Gait
        generator_ = std::make_shared<GaitGeneratorBipedFeet3D>();

        // The order of feet parts need to match with the gait-generator
        std::vector<std::string> ee_names = {"FootHeelFixed_L", "FootToesOFixed_L", "FootToesIFixed_L",
                                             "FootHeelFixed_R", "FootToesOFixed_R", "FootToesIFixed_R"};
        robot_model_ = std::make_shared<RaiSimRobotModel>("resources/we2/we2.urdf", size_q_, size_dq_, size_u_,
                                                          ee_names);
    }

} /* namespace gambol */

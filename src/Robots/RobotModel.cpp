#include <gambol/Robots/RobotModel.h>

namespace gambol {

    RobotModel::RobotModel(int size_q, int size_dq, int size_u, int ee_count) {
        size_q_ = size_q;
        size_dq_ = size_dq;
        size_u_ = size_u;
        ee_count_ = ee_count;

        // Allocate vectors and matrices
        q_ = VectorXd::Zero(size_q_);
        dq_ = VectorXd::Zero(size_dq_);
        u_ = VectorXd::Zero(size_u_);
        ee_forces_ = std::vector<VectorXd>(ee_count, VectorXd::Zero(3)); // Allocate
    }

    bool RobotModel::HasIK() const {
        return false;
    }

    void RobotModel::SetCurrent(const VectorXd& q, const VectorXd& dq,
                                const VectorXd& u, const std::vector<VectorXd>& ee_forces) {
        if (q.size() > 0) {
            q_ = q;

#if NORMALIZE_QUATERNIONS
            if (!quat_ids_.empty()) {
                for (auto i : quat_ids_) {
                    q_.segment(i, 4).normalize(); // Perform in-place normalization
                }
            }
#endif
        }
        if (dq.size() > 0) {
            dq_ = dq;
        }
        if (u.size() > 0) {
            u_ = u;
        }
        if (!ee_forces.empty()) {
            ee_forces_ = ee_forces;
        }

        Update();
    }

    void RobotModel::SetCurrent(const NodesHolder& s, int k) {
        std::vector<VectorXd> ee_forces;

        for (int ee = 0; ee < GetEECount(); ee++) {
            ee_forces.push_back(s.ee_forces_[ee]->GetNode(k));
        }

        SetCurrent(s.joint_pos_->GetNode(k), s.joint_vel_->GetNode(k),
                   s.torques_->GetNode(k), ee_forces);
    }

    void RobotModel::SetCurrentTime(const NodesHolder& s, double t) {
        std::vector<VectorXd> ee_forces;

        for (int ee = 0; ee < GetEECount(); ee++) {
            ee_forces.push_back(s.ee_forces_[ee]->GetPoint(t));
        }

        SetCurrent(s.joint_pos_->GetPoint(t), s.joint_vel_->GetPoint(t),
                   s.torques_->GetPoint(t), ee_forces);
    }

    int RobotModel::GetEECount() const {
        return ee_count_;
    }

    RobotModel::VectorXd RobotModel::GetRatesFromVel() const {
        if (size_q_ == size_dq_) {
            return dq_;
        }

        VectorXd q_dot(size_q_); // Joint rates (derivative of gen. coord.)

        uint quat_id = 0; // Index of quat
        int i_v = 0; // Index of gen. velocity

        for (int i_q = 0; i_q < size_q_; i_q++) {
            // If not first item of quaternion
            if (quat_ids_.empty() || quat_ids_[quat_id] != i_q) {
                q_dot[i_q] = dq_[i_v++];
                continue;
            }

            Eigen::Vector4d quat = q_.segment(i_q, 4);
            Eigen::Vector3d omega = dq_.segment(i_v, 3);

            // Angular velocity to quaternion derivative
            q_dot.segment(i_q, 4) = QuaternionDerivative(quat, omega);
            i_q += 3; // And +1 from the for-loop
            i_v += 3;
            if (quat_id < quat_ids_.size() - 1) // Do not exceed vector
            {
                quat_id++;
            }
        }

        return q_dot;
    }

    RobotModel::Jacobian RobotModel::GetRatesJacobianWrtPos() const {
        if (size_q_ == size_dq_) {
            Jacobian jac(size_q_, size_q_); // Empty
            jac.setZero();
            return jac;
        }

        uint quat_id = 0; // Index of quat
        int i_v = 0; // Index in gen. velocity
        int i_q; // Index in gen. coord.

        Jacobian qpos_jac(size_q_, size_q_);

        // Row of jacobian
        for (i_q = 0; i_q < size_q_; i_q++) {
            // If not first item of quaternion
            if (quat_ids_.empty() || quat_ids_[quat_id] != i_q) {
                // qpos derivative not dependent on qpos (= 0, so no .insert() )
                i_v++;
                continue;
            }

            MatrixXd omega_M = AngularVelocityMatrix(dq_.segment(i_v, 3));

            for (int r = 0; r < 4; r++) {
                for (int c = 0; c < 4; c++) {
                    qpos_jac.insert(i_q + r, i_q + c) = 0.5 * omega_M(r, c);
                }
            }

            i_q += 3; // And +1 from the for-loop
            i_v += 3;
            if (quat_id < quat_ids_.size() - 1) // Do not exceed vector
            {
                quat_id++;
            }
        }

        assert(i_v == size_dq_); // Make sure we had all
        assert(i_q == size_q_);

        return qpos_jac;
    }

    RobotModel::Jacobian RobotModel::GetRatesJacobianWrtVel() const {
        if (size_q_ == size_dq_) {
            Jacobian jac(size_q_, size_q_);
            jac.setIdentity();
            return jac;
        }

        uint quat_id = 0; // Index of quat
        int i_v = 0; // Index in gen. velocity
        int i_q; // Index in gen. coord.

        Jacobian qpos_jac(size_q_, size_dq_);

        // Row of jacobian
        for (i_q = 0; i_q < size_q_; i_q++) {
            // If not first item of quaternion
            if (quat_ids_.empty() || quat_ids_[quat_id] != i_q) {
                qpos_jac.insert(i_q, i_v++) = 1.0; // Identity part
                continue;
            }

            MatrixXd quat_M = QuaternionMatrix(q_.segment(i_q, 4));

            for (int r = 0; r < 4; r++) {
                for (int c = 0; c < 3; c++) {
                    qpos_jac.insert(i_q + r, i_v + c) = 0.5 * quat_M(r, c);
                }
            }

            i_q += 3; // And +1 from the for-loop
            i_v += 3;
            if (quat_id < quat_ids_.size() - 1) // Do not exceed vector
            {
                quat_id++;
            }
        }

        assert(i_v == size_dq_); // Make sure we had all
        assert(i_q == size_q_);

        return qpos_jac;
    }

    RobotModel::Jacobian RobotModel::GetVelJacobianWrtRates() const {
        if (size_q_ == size_dq_) {
            return GetRatesJacobianWrtVel(); // Just identity
        }

        uint quat_id = 0; // Index of quat
        int i_v; // Index of qvel
        int i_q = 0; // Index of qpos

        Jacobian qpos_jac(size_dq_, size_q_);

        // Row of jacobian
        for (i_v = 0; i_v < size_dq_; i_v++) {
            // If not first item of quaternion
            if (quat_ids_.empty() || quat_ids_[quat_id] != i_q) {
                qpos_jac.insert(i_v, i_q++) = 1.0; // Identity part
                continue;
            }

            MatrixXd quat_M = QuaternionMatrixTranspose(q_.segment(i_q, 4));

            for (int r = 0; r < 3; r++) {
                for (int c = 0; c < 4; c++) {
                    qpos_jac.insert(i_q + r, i_v + c) = 2.0 * quat_M(r, c);
                }
            }

            i_q += 4;
            i_v += 2; // And +1 from the for-loop
            if (quat_id < quat_ids_.size() - 1) // Do not exceed vector
            {
                quat_id++;
            }
        }

        assert(i_v == size_dq_); // Make sure we had all
        assert(i_q == size_q_);

        return qpos_jac;
    }

    const std::vector<int>& RobotModel::GetQuatIndices() const {
        return quat_ids_;
    }

    double RobotModel::GetTotalMass() const {
        return 1.0; // Default
    }

    double RobotModel::GetGravity() const {
        return 9.81; // Default
    }

    RobotModel::Jacobian RobotModel::DenseToSparse(Eigen::MatrixXd mat) {
        int rows = mat.rows(), cols = mat.cols();
        Jacobian jac(rows, cols);

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                jac.insert(r, c) = mat(r, c);
            }
        }

        return jac;
    }

    RobotModel::Vector3d RobotModel::GetBasePos() const {
        assert(false && "Not implemented");
        // return Vector3d::Zero();
    }

    RobotModel::Jacobian RobotModel::GetBasePosJacobian() const {
        assert(false && "Not implemented");
        // Jacobian jac(3, size_q_);
        // return jac;
    }

    RobotModel::VectorXd RobotModel::GetInverseKinematics(const Vector3d&,
                                                          const Vector4d&, const std::vector<Vector3d>&) const {
        assert(false && "Not implemented");
        // return VectorXd::Zero(size_q_);
    }


    Eigen::Vector4d RobotModel::QuaternionDerivative(const Vector4d& q, const Vector3d& omega) const {
        return 0.5 * QuaternionMatrix(q) * omega;
    }

    Eigen::MatrixXd RobotModel::QuaternionMatrix(const Vector4d& q) const {
        MatrixXd quat_M(4, 3);

        // "G^T" in https://arxiv.org/pdf/0811.2889.pdf
        quat_M << -q[1], -q[2], -q[3], // row 1
                q[0], -q[3], q[2], // row 2
                q[3], q[0], -q[1], // row 3
                -q[2], q[1], q[0]; // row 4

        return quat_M;
    }

    Eigen::MatrixXd RobotModel::QuaternionMatrixTranspose(const Vector4d& q) const {
        return QuaternionMatrix(q).transpose();
    }

    Eigen::MatrixXd RobotModel::AngularVelocityMatrix(Vector3d w) const {
        MatrixXd omega_M(4, 4);

        omega_M << 0, -w[0], -w[1], -w[2], // row 1
                w[0], 0, w[2], -w[1], // row 2
                w[1], -w[2], 0, w[0], // row 3
                w[2], w[1], -w[0], 0; // row 4

        return omega_M;
    }

} /* namespace gambol */

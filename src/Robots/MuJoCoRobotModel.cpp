#include <string>
#include <cassert>
#include <mujoco/mujoco.h>
#include <gambol/Robots/MuJoCoRobotModel.h>

namespace gambol {

    MuJoCoRobotModel::MuJoCoRobotModel(const std::string& file, int size_q,
                                       int size_dq, int size_u, const std::vector<std::string>& ee_names) :
            RobotModel(size_q, size_dq, size_u, ee_names.size()),
            model_(file) {
        mjModel* m = model_.get_model();

        // Disable contact dynamics
        model_.enable_contact(false);

        assert(m->nq == size_q_); // Make sure nDOF is correct
        assert(m->nv == size_dq_);
        assert(m->nu == size_u_);

        for (const auto& name : ee_names) {
            int id = model_.get_body_id(name);
            assert(id > 0); // Not negative and not the world body
            ee_body_id_.push_back(id);
        }

        base_body_id_ = -1; // Not defined
    }

    MuJoCoRobotModel::Ptr MuJoCoRobotModel::clone() const {
        return std::make_shared<MuJoCoRobotModel>(*this);
    }

    bool MuJoCoRobotModel::HasIK() const {
        return base_body_id_ > 0;
    }

    double MuJoCoRobotModel::GetTotalMass() const {
        return mj_getTotalmass(model_.get_model());
    }

    double MuJoCoRobotModel::GetGravity() const {
        return mju_norm3(model_.get_model()->opt.gravity);
    }

    const std::vector<int>& MuJoCoRobotModel::GetEEBodyIds() const {
        return ee_body_id_;
    }

    MuJoCoModel& MuJoCoRobotModel::GetMuJoCoModel() {
        return model_;
    }

    void MuJoCoRobotModel::Update() {
        std::vector<std::pair<int, VectorXd>> xfrc(GetEECount());

        for (int ee = 0; ee < GetEECount(); ee++) {
            VectorXd force(6);
            force << ee_forces_[ee], VectorXd::Zero(3);

            xfrc[ee].first = ee_body_id_[ee];
            xfrc[ee].second = force;
        }

        // Bring model to current instance
        model_.reset_data(q_, dq_, u_, xfrc);
        // Quaternions are normalized inside function
    }

    MuJoCoRobotModel::VectorXd MuJoCoRobotModel::GetDynamics() const {
        VectorXd ddq_calculated = model_.get_dynamics();

        return ddq_calculated;
    }

    MuJoCoRobotModel::Jacobian MuJoCoRobotModel::GetDynamicsJacobianWrtPos() const {
        MatrixXd ddq_diff_qpos = model_.get_dynamics_diff_qpos();

        //return ddq_diff_qpos.sparseView();
        return DenseToSparse(ddq_diff_qpos);
    }

    MuJoCoRobotModel::Jacobian MuJoCoRobotModel::GetDynamicsJacobianWrtVel() const {
        MatrixXd ddq_diff_qvel = model_.get_dynamics_diff_qvel();

        //return ddq_diff_qvel.sparseView();
        return DenseToSparse(ddq_diff_qvel);
    }

    MuJoCoRobotModel::Jacobian MuJoCoRobotModel::GetDynamicsJacobianWrtTorque() const {
        MatrixXd ddq_diff_u = model_.get_dynamics_diff_u();

        //return ddq_diff_u.sparseView();
        return DenseToSparse(ddq_diff_u);
    }

    MuJoCoRobotModel::Jacobian MuJoCoRobotModel::GetDynamicsJacobianWrtForces(
            uint ee_id) const {
        MatrixXd ddq_diff_f = model_.get_dynamics_diff_xfrc(ee_body_id_[ee_id],
                                                            MuJoCoModel::FRC_FORCE);

        //return ddq_diff_u.sparseView();
        return DenseToSparse(ddq_diff_f);
    }

    MuJoCoRobotModel::Vector3d MuJoCoRobotModel::GetEEPos(uint ee_id) const {
        int id = ee_body_id_[ee_id];

        return model_.get_position(id);
    }

    MuJoCoRobotModel::Jacobian MuJoCoRobotModel::GetEEPosJacobian(uint ee_id) const {
        int id = ee_body_id_[ee_id];
        MatrixXd jac_qvel = model_.get_geometric_jacobian_pos(id); // Velocity jacobian, not rates jacobian!

        // J_q(q) = J_v(q) * B^-1(q)
        return DenseToSparse(jac_qvel) * GetVelJacobianWrtRates();
    }

    MuJoCoRobotModel::Vector3d MuJoCoRobotModel::GetBasePos() const {
        return model_.get_position(base_body_id_);
    }

    MuJoCoRobotModel::Jacobian MuJoCoRobotModel::GetBasePosJacobian() const {
        MatrixXd jac = model_.get_position_diff_qpos(base_body_id_);

        return DenseToSparse(jac);
    }

    MuJoCoRobotModel::VectorXd MuJoCoRobotModel::GetInverseKinematics(
            const Vector3d& base_pos, const Vector4d& base_rot,
            const std::vector<Vector3d>& ee_pos) const {
        MuJoCoModel::ListVector3d ee_refs;

        ee_refs.push_back({base_body_id_, base_pos});

        for (uint ee = 0; ee < ee_pos.size(); ee++) {
            ee_refs.push_back({ee_body_id_[ee], ee_pos[ee]});
        }

        MuJoCoModel::ListVector4d quat_refs;
        quat_refs.push_back(
                {base_body_id_, base_rot});

        VectorXd qpos(size_q_);

        auto settings = MuJoCoModel::IKSettings();
        settings.iter_max = 30;

        bool result = model_.inverse_kinematics(ee_refs, quat_refs, settings);
        (void) result;

        mju_copy(qpos.data(), model_.get_data()->qpos, size_q_);

        return qpos;
    }

} /* namespace gambol */

#include <gambol/Costs/NodeCost.h>

namespace gambol {

    // Constructor
    NodeCost::NodeCost(const std::string& nodes_id, int dim, double weight,
                       double exp, const std::string& prefix) :
            CostTerm(prefix + nodes_id + "-dim_" + std::to_string(dim)) {
        node_id_ = nodes_id;
        dim_ = dim;
        weight_ = weight;
        exp_ = exp;
    }

    // Link to optimization variable
    void NodeCost::InitVariableDependedQuantities(const VariablesPtr& x) {
        nodes_ = x->GetComponent<NodesVariables>(node_id_);
    }

    // Calculate cost value
    double NodeCost::GetCost() const {
        double cost = 0.0;
        for (auto node : nodes_->GetNodes()) {
            double val = node[dim_];
            cost += weight_ * std::pow(val, exp_); // Square
        }

        return cost;
    }

    // Calculate part of jacobian
    void NodeCost::FillJacobianBlock(std::string var_set, Jacobian& jac) const {
        if (var_set == node_id_) {
            for (int i = 0; i < nodes_->GetRows(); i++) {
                auto nvi = nodes_->GetNodeInfo(i);

                if (nvi.dim_ == dim_) {
                    double val = nodes_->GetNode(nvi.id_)[dim_];
                    jac.coeffRef(0, i) = weight_ * exp_ * std::pow(val, exp_ - 1.0);
                }
            }
        }
    }

} /* namespace gambol */

<!--
	This model consists of a five link pendulum tree, to make up a basic walker
	Feet are included also, as seperate geoms
	The model is free in 3D
	
-->
<mujoco model="FiveLinkBiped">

	<compiler inertiafromgeom="true" angle="radian"/>

	<!-- Defaults -->
	<default>
		<joint damping="0.2" armature="0"/>

		<geom condim="1" material="matgeom"/>

		<motor ctrlrange="-200 200" ctrllimited="true"/>

		<!-- Upper leg -->
		<default class="upper_leg1">
			<joint type="hinge" axis="0 1 0" pos="0 0 0.4" limited="true" range="-1.5 1.5" />
			<geom type="capsule" fromto="0 0 0 0 0 0.4" size="0.05 0.3" />
		</default>
		<default class="upper_leg2">
			<joint type="hinge" axis="1 0 0" pos="0 0 0.4" limited="true" range="-0.1 0.5" />
			<geom type="sphere" pos="0.01 0 0" size="0.05" />
		</default>
		<default class="upper_leg3">
			<joint type="hinge" axis="0 0 1" pos="0 0 0.4" limited="true" range="-0.5 0.5" />
		</default>

		<!-- Lower leg -->
		<default class="lower_leg">
			<joint type="hinge" axis="0 1 0" pos="0 0 0.4" limited="true" range="0.01 2.5" />
			<geom type="capsule" fromto="0 0 0.05 0 0 0.4" size="0.05 0.3"/>
		</default>

		<!-- Foot -->
		<default class="foot1">
			<joint type="hinge" axis="0 1 0" pos="0 0 0" limited="true" range="-1.0 1.0" />
			<geom type="box" size="0.125 0.04 0.01" pos="0.075 0 0.01"/>
		</default>
		<default class="foot2">
			<joint type="hinge" axis="1 0 0" pos="0 0 0" limited="true" range="-0.5 0.5" />
			<geom type="sphere" size="0.02" pos="0.2 0 0."/>
		</default>

	</default>

	<visual>
		<quality shadowsize="2048"/>
	</visual>

	<asset>
		<texture type="skybox" builtin="gradient" rgb1="0.3 0.5 0.7" rgb2="0.2 0.2 0.2" width="512" height="512"/> 
		<texture name="texplane" type="2d" builtin="checker" rgb1=".2 .3 .4" rgb2=".1 0.15 0.2" width="512" height="512" mark="cross" markrgb=".8 .8 .8"/>  
		
		<material name="matplane" texture="texplane" texrepeat="1 1" texuniform="true"/>
		<material name="matgeom" rgba="0.1 0.8 0.1 1"/>

		<hfield name="terrainhfield" nrow="31" ncol="31" size="2 2 0.3 0.1" />
	</asset>
	
	<worldbody>
        <light directional="false" diffuse="0.2 0.2 0.2" specular="0 0 0" pos="0 0 5" dir="0 0 -1" castshadow="false"/>
        <light mode="targetbodycom" target="base" directional="false" diffuse="0.6 0.6 0.6" specular="0.3 0.3 0.3" pos="-0.5 0.5 2.0" dir="0 0 -1"/>

		<geom name="terrain" pos="0 0 0.00001" type="hfield" hfield="terrainhfield" material="matplane" condim="3"/>

		<!-- Base -->
		<body name="base" pos="0 0 0.82">
			<freejoint />

			<geom type="box" pos="0 0 0.35" size="0.06 0.06 0.35"/>
			<geom type="sphere" size="0.025" pos="0.06 0.04 0.4" />
			<geom type="sphere" size="0.025" pos="0.06 -0.04 0.4" />

			<!-- Left upper leg -->
			<body name="left_upper_leg" pos="0 0.11 -0.4">
				<joint class="upper_leg1" name="hinge_hip_left_y"/>
				<joint class="upper_leg2" name="hinge_hip_left_x"/>
				<joint class="upper_leg3" name="hinge_hip_left_z"/>

				<geom class="upper_leg1" />
				<geom class="upper_leg2" />

				<!-- Left lower leg -->
				<body name="left_lower_leg" pos="0 0 -0.4">
					<joint class="lower_leg" name="hinge_knee_left" />
					<geom class="lower_leg" />

					<!-- Left foot -->
					<body name="left_foot" pos="0 0 0">
						<joint class="foot1" name="hinge_ankle_left_y" />
						<joint class="foot2" name="hinge_ankle_left_x" />

						<geom class="foot1" />
						<geom class="foot2" />

						<body name="left_heel" pos="-0.05 0 -0.02">
							<geom type="sphere" size="0.04" pos="0 0 0.04"/>
						</body>
						<body name="left_toes_l" pos="0.2 0.03 -0.02">
							<geom type="sphere" size="0.02" pos="0 0 0.02"/>
						</body>
						<body name="left_toes_r" pos="0.2 -0.03 -0.02">
							<geom type="sphere" size="0.02" pos="0 0 0.02"/>
						</body>

					</body>				
				</body>
			</body>

			<!-- Right upper leg -->
			<body name="right_upper_leg" pos="0 -0.11 -0.4">
				<joint class="upper_leg1" name="hinge_hip_right_y"/>
				<joint class="upper_leg2" axis="-1 0 0" name="hinge_hip_right_x"/>
				<joint class="upper_leg3" axis="0 0 -1" name="hinge_hip_right_z"/>

				<geom class="upper_leg1" />
				<geom class="upper_leg2" />

				<!-- Right lower leg -->
				<body name="right_lower_leg" pos="0 0 -0.4">
					<joint class="lower_leg" name="hinge_knee_right" />
					<geom class="lower_leg" />

						<!-- Right foot -->
						<body name="right_foot" pos="0 0 0">
							<joint class="foot1" name="hinge_ankle_right_y" />
							<joint class="foot2" axis="-1 0 0" name="hinge_ankle_right_x" />

							<geom class="foot1" />
							<geom class="foot2" />

							<body name="right_heel" pos="-0.05 0 -0.02">
								<geom type="sphere" size="0.04" pos="0 0 0.04"/>
							</body>
							<body name="right_toes_l" pos="0.2 -0.03 -0.02">
								<geom type="sphere" size="0.02" pos="0 0 0.02"/>
							</body>
							<body name="right_toes_r" pos="0.2 0.03 -0.02">
								<geom type="sphere" size="0.02" pos="0 0 0.02"/>
							</body>

						</body>
				</body>
			</body>

		</body>

	</worldbody>

	<actuator>
		<motor joint="hinge_hip_left_y" />
		<motor joint="hinge_hip_left_x" />
		<motor joint="hinge_hip_left_z" />
		<motor joint="hinge_hip_right_y" />
		<motor joint="hinge_hip_right_x" />
		<motor joint="hinge_hip_right_z" />

		<motor joint="hinge_knee_left" />
		<motor joint="hinge_knee_right" />

		<motor joint="hinge_ankle_left_y" />
		<motor joint="hinge_ankle_left_x" />
		<motor joint="hinge_ankle_right_y" />
		<motor joint="hinge_ankle_right_x" />
	</actuator>

</mujoco>
